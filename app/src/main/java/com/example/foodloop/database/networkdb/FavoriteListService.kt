package com.example.foodloop.database.networkdb

import com.google.firebase.Firebase
import com.google.firebase.firestore.firestore
import okio.withLock
import java.util.concurrent.locks.ReentrantLock

interface FavoriteNetService{
    companion object{
        suspend fun getFavoriteList(): NetFavoriteContainer{
            val db = Firebase.firestore
            val favorites: MutableList<NetFavorite> = mutableListOf()
            val i = 0

            val lock = ReentrantLock()
            val condition = lock.newCondition()

            db.collection("Favoritess")
                .get()
                .addOnSuccessListener { favorites_firebase ->
                    for(favorite_fb in favorites_firebase){
                        favorites.add(
                            i, NetFavorite(
                                userId = favorite_fb.data["user_id"].toString(),
                                productId = favorite_fb.data["product_id"].toString()
                            )
                        )
                    }
                    lock.withLock { condition.signal() }
                }
            lock.withLock {
                condition.await()
                return NetFavoriteContainer(favorites)
            }
        }
    }
}