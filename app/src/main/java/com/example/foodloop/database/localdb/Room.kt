package com.example.foodloop.database.localdb

import android.app.Application
import androidx.room.Dao
import androidx.room.Database
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.foodloop.database.repositories.DBRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

@Dao
interface DatabaseDao{
    // Product
    @Query("SELECT * FROM DatabaseProduct")
    fun getProducts(): Flow<List<DatabaseProduct>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun  insertAllProducts(products: List<DatabaseProduct>): LongArray

    // User
    @Query("SELECT * FROM DatabaseUser")
    fun getUsers(): Flow<List<DatabaseUser>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun  insertAllUsers(products: List<DatabaseUser>): LongArray

    // Favorite
    @Query("SELECT * FROM DatabaseFavorite")
    fun getFavorites(): Flow<List<DatabaseFavorite>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAllFavorites(favorites: List<DatabaseFavorite>) : LongArray
}

@Database(entities = arrayOf(DatabaseUser::class, DatabaseProduct::class, DatabaseFavorite::class), version = 7)
abstract class DatabaseRoom: RoomDatabase(){

    abstract fun databaseDao(): DatabaseDao
    private class RoomDatabaseCallback(private val scope: CoroutineScope): RoomDatabase.Callback(){
        override fun onCreate(db: SupportSQLiteDatabase){
            super.onCreate(db)
            INSTANCE?.let{
                database ->
                scope.launch { populateDatabase(database) }
            }
        }

        suspend fun populateDatabase(database: DatabaseRoom){
            DBRepository(database).refreshData()
        }
    }

    companion object{
        @Volatile
        private var INSTANCE: DatabaseRoom ? = null
        fun getDataBase(context: Application, scope: CoroutineScope): DatabaseRoom {
            return INSTANCE ?: synchronized(this){
                val instance = Room.databaseBuilder(context,
                    DatabaseRoom::class.java,
                    "database_room.db")
                    .allowMainThreadQueries()
                    .addCallback(RoomDatabaseCallback(scope))
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }

}