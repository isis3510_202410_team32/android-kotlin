package com.example.foodloop.database.networkdb
import com.example.foodloop.database.localdb.DatabaseFavorite
import com.example.foodloop.database.localdb.DatabaseProduct
import com.example.foodloop.database.localdb.DatabaseUser
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class NetProductContainer(val products: List<NetProduct>)
@JsonClass(generateAdapter = true)
data class NetProduct(
    val id: String,
    val name: String,
    val description: String,
    val price: Int,
    val discountPrice: Int,
    val imageUrl: String
)

@JsonClass(generateAdapter = true)
data class NetUserContainer(val users: List<NetUser>)
@JsonClass(generateAdapter = true)
data class NetUser(
    val id: String,
    val name: String,
    val email: String,
    val password: String
)

@JsonClass(generateAdapter = true)
data class NetFavoriteContainer(val favorites: List<NetFavorite>)

@JsonClass(generateAdapter = true)
data class NetFavorite(
    val userId: String,
    val productId: String
)

fun NetUserContainer.asDatabaseModel(): List<DatabaseUser>{
    return users.map {
        DatabaseUser(
            id = it.id,
            name = it.name,
            email = it.email,
            password = it.password
        )
    }
}

fun NetProductContainer.asDatabaseModel():List<DatabaseProduct>{
    return products.map{
        DatabaseProduct(
            id = it.id,
            name = it.name,
            description = it.description,
            price = it.price,
            discountPrice = it.discountPrice,
            imageUrl = it.imageUrl
        )
    }
}
fun NetFavoriteContainer.asDataModel(): List<DatabaseFavorite>{
    return favorites.map {
        DatabaseFavorite(
            userId = it.userId,
            productId = it.productId
        )
    }
}