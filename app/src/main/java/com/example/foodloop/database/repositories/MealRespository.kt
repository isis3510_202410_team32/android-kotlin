package com.example.foodloop.database.repositories

import com.example.foodloop.database.networkdb.MealApiService
import com.example.foodloop.model.DTO.Meal
import com.example.foodloop.model.DTO.MealResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MealRepository {
    private val api: MealApiService = getRetrofit().create(MealApiService::class.java)

    suspend fun getMealsByIngredient(ingredient: String): List<Meal>? {
        return withContext(Dispatchers.IO) {
            try {
                val response: Response<MealResponse> = api.getMealsByIngredient(ingredient)
                if (response.isSuccessful) {
                    response.body()?.meals
                } else {
                    emptyList()
                }
            } catch (e: Exception) {
                emptyList()
            }
        }
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://www.themealdb.com/api/json/v1/1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}

