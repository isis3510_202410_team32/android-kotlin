package com.example.foodloop.database.repositories

import androidx.room.withTransaction
import com.example.foodloop.database.localdb.DatabaseRoom
import com.example.foodloop.database.localdb.asDomainModel
import com.example.foodloop.domain.Favorite
import com.example.foodloop.domain.User
import com.example.foodloop.domain.Product1
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class DBRepository(private val database: DatabaseRoom){
    val products: Flow<List<Product1>> = database.databaseDao().getProducts().map {
        originalList -> originalList.asDomainModel()
    }
    val users: Flow<List<User>> = database.databaseDao().getUsers().map {
            originalList -> originalList.asDomainModel()
    }
    val favorites: Flow<List<Favorite>> = database.databaseDao().getFavorites().map {
            originalList -> originalList.asDomainModel()
    }

    suspend fun refreshData(){
        database.withTransaction {

        }
    }
}