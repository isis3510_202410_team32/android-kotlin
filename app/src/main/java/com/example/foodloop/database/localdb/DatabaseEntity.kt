package com.example.foodloop.database.localdb

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.foodloop.domain.Favorite
import com.example.foodloop.domain.Product1
import com.example.foodloop.domain.User

@Entity
data class DatabaseUser(
    @PrimaryKey
    var id: String = "",
    var email: String = "",
    var name: String = "",
    var password: String = ""
)

@Entity
data class DatabaseProduct(
    @PrimaryKey
    var id: String = "",
    var name: String = "",
    var description: String = "",
    var price: Int = 0,
    var discountPrice: Int = 0,
    var imageUrl: String = ""
)

@Entity(primaryKeys = ["userId", "productId"])
data class DatabaseFavorite(
    var userId: String = "",
    val productId: String = ""
)

@JvmName("productAsDomainModel")
fun List<DatabaseProduct>.asDomainModel(): List<Product1>{
    return map{
        Product1(
            id = it.id,
            name = it.name,
            description = it.description,
            price = it.price,
            discountPrice = it.discountPrice,
            imageUrl = it.imageUrl
        )
    }
}

@JvmName("userAsDomainModel")
fun List<DatabaseUser>.asDomainModel(): List<User>{
    return map{
        User(
            id = it.id,
            email = it.email,
            name = it.name,
            password = it.password
        )
    }
}

@JvmName("favoriteAsDomainModel")
fun List<DatabaseFavorite>.asDomainModel(): List<Favorite>{
    return map {
        Favorite(
            userId = it.userId,
            productId = it.productId
        )
    }
}