package com.example.foodloop.database.networkdb

import com.example.foodloop.model.DTO.MealResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MealApiService {
    @GET("filter.php")
    suspend fun getMealsByIngredient(@Query("i") ingredient: String):Response<MealResponse>
}