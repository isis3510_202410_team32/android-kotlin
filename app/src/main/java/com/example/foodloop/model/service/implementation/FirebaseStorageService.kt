package com.example.foodloop.model.service.implementation

import android.util.Log
import com.example.foodloop.model.Purchase
import com.example.foodloop.model.service.StorageService
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.tasks.await


class FirebaseStorageService : StorageService {
    override fun getChartData(onError: (Throwable) -> Unit, onSuccess: (Array<Float>) -> Unit) {
        val db = FirebaseFirestore.getInstance()

        db.collection("purchase")
            .whereEqualTo("userId", "4hGdoRo0AORn9Q4o3TP9")
            .get()
            .addOnSuccessListener {
                    result ->
                val chartData = Array(12){0F}

                for(document in result){
                    val purchase = document.toObject(Purchase::class.java)
                    val month = purchase.purchaseDate.split("/")[1].toInt()
                    chartData[month-1] += purchase.discount
                }
                onSuccess(chartData)
                Log.d("Firestore", "saviings: ${chartData[0]}, ${chartData[1]}, ${chartData[2]}, ${chartData[3]}, ${chartData[4]}, ${chartData[5]}, ${chartData[6]}, ${chartData[7]}, ${chartData[8]}, ${chartData[9]}, ${chartData[10]}, ${chartData[11]}")
            }
            .addOnFailureListener {
                Log.d("Firestore", "Noooooooooooo")
            }

        Log.d("Firestore", "GET")
    }

}