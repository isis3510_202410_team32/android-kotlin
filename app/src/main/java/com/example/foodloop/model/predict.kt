package com.example.foodloop.model

class Predict {
    var id: String = ""
    var count: Double = 0.0
    var date: String = ""

    constructor()

    constructor(id: String, count: Double, date: String) {
        this.id = id
        this.count = count
        this.date = date
    }
}
