package com.example.foodloop.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.example.foodloop.model.LocalFoodWaste

class DatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "local_food_waste.db"
        private const val DATABASE_VERSION = 1
        private const val TABLE_NAME = "local_food_waste"
        private const val COLUMN_ID = "id"
        private const val COLUMN_NAME = "name"
        private const val COLUMN_FOOD_TYPE = "food_type"
        private const val COLUMN_WEIGHT = "weight"

        private const val SQL_CREATE_ENTRIES =
            "CREATE TABLE $TABLE_NAME (" +
                    "$COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                    "$COLUMN_NAME TEXT NOT NULL," +
                    "$COLUMN_FOOD_TYPE TEXT NOT NULL," +
                    "$COLUMN_WEIGHT REAL NOT NULL)"

        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS $TABLE_NAME"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    fun insertLocalFoodWaste(name: String, foodType: String, weight: Double): Boolean {
        val db = writableDatabase
        val values = ContentValues().apply {
            put(COLUMN_NAME, name)
            put(COLUMN_FOOD_TYPE, foodType)
            put(COLUMN_WEIGHT, weight)
        }
        val newRowId = db.insert(TABLE_NAME, null, values)
        return newRowId != -1L
    }

    fun getAllLocalFoodWaste(): List<LocalFoodWaste> {
        val db = readableDatabase
        val cursor = db.query(
            TABLE_NAME,
            arrayOf(COLUMN_ID, COLUMN_NAME, COLUMN_FOOD_TYPE, COLUMN_WEIGHT),
            null,
            null,
            null,
            null,
            null
        )
        val wastes = mutableListOf<LocalFoodWaste>()
        with(cursor) {
            while (moveToNext()) {
                val id = getInt(getColumnIndexOrThrow(COLUMN_ID))
                val name = getString(getColumnIndexOrThrow(COLUMN_NAME))
                val foodType = getString(getColumnIndexOrThrow(COLUMN_FOOD_TYPE))
                val weight = getDouble(getColumnIndexOrThrow(COLUMN_WEIGHT))
                wastes.add(LocalFoodWaste(id, name, foodType, weight))
            }
        }
        cursor.close()
        return wastes
    }
}
