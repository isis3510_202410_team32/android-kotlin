package com.example.foodloop.model
data class Product(
    val description: String = "",
    val discountPrice: String = "",
    val expirationDate: String = "",
    val imageUrl: String = "",
    val name: String = "",
    val price: String = "",
    val id: String = ""
)
