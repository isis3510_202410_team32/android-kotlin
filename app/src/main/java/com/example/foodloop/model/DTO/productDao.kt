package com.example.foodloop.domain

import com.example.foodloop.model.Product
import com.google.firebase.firestore.FirebaseFirestore

class ProductDao {

    private val db = FirebaseFirestore.getInstance()
    private val productsCollection = db.collection("products")
    private val favsCollection = db.collection("favs") //

    fun getProducts(callback: (List<Product>?, Exception?) -> Unit) {
        productsCollection.get()
            .addOnSuccessListener { result ->
                val products = result.documents.mapNotNull { document ->
                    try {
                        document.toObject(Product::class.java)?.copy(id = document.id)
                    } catch (e: Exception) {
                        null
                    }
                }

                callback(products, null)
            }
            .addOnFailureListener { exception ->
                callback(null, exception)
            }
    }

    fun getProductsByEmail(email: String, callback: (List<Product>?, Exception?) -> Unit) {
        val db = FirebaseFirestore.getInstance()
        db.collection("users")
            .whereEqualTo("email", email)
            .get()
            .addOnSuccessListener { documents ->
                if (documents.isEmpty) {
                    callback(emptyList(), null)
                } else {
                    val userId = documents.documents[0].id

                    db.collection("users").document(userId).collection("favorites")
                        .get()
                        .addOnSuccessListener { favorites ->
                            val products = favorites.documents.mapNotNull { doc ->
                                doc.toObject(Product::class.java)
                            }
                            callback(products, null)
                        }
                        .addOnFailureListener { exception ->
                            callback(null, exception)
                        }
                }
            }
            .addOnFailureListener { exception ->
                callback(null, exception)
            }
    }
}

