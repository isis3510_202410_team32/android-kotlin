package com.example.foodloop.ui

import android.content.Context
import android.util.Log
import com.example.foodloop.model.Predict
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.concurrent.TimeUnit

class ExpiryCache(private val context: Context, private val maxSize: Int) {
    private data class CacheEntry(val key: String, val value: List<Predict>, val timestamp: Long)

    private val cache = ArrayDeque<CacheEntry>()
    private val logTag = "ExpiryCache"
    private val sharedPreferences = context.getSharedPreferences("ExpiryCache", Context.MODE_PRIVATE)
    private val gson = Gson()
    private val expirationTime = TimeUnit.HOURS.toMillis(24)

    init {
        loadCache()
    }

    private fun loadCache() {
        val cacheJson = sharedPreferences.getString("cache", null)
        if (cacheJson != null) {
            try {
                val type = object : TypeToken<ArrayDeque<CacheEntry>>() {}.type
                val loadedCache: ArrayDeque<CacheEntry> = gson.fromJson(cacheJson, type)
                cache.clear()
                cache.addAll(loadedCache)
                Log.d(logTag, "Loaded cache from SharedPreferences: $loadedCache")
            } catch (e: Exception) {
                Log.e(logTag, "Failed to load cache from SharedPreferences", e)
            }
        }
    }

    fun put(key: String, value: List<Predict>?) {
        if (value == null) {
            Log.d(logTag, "Attempted to add null value for key: $key")
            return
        }

        val currentTime = System.currentTimeMillis()
        cache.removeIf { it.key == key }
        if (cache.size >= maxSize) {
            val removed = cache.removeFirst()
            Log.d(logTag, "Removed oldest item from cache: ${removed.key} -> ${removed.value}")
        }
        cache.addLast(CacheEntry(key, value, currentTime))
        saveCache()
        Log.d(logTag, "Added item to cache: $key -> $value")
    }

    fun get(key: String): List<Predict>? {
        val currentTime = System.currentTimeMillis()
        loadCache() // Ensure cache is reloaded from SharedPreferences every time get is called

        val item = cache.find { it.key == key }
        return if (item != null) {
            if (currentTime - item.timestamp < expirationTime) {
                cache.remove(item)
                cache.addLast(item)
                saveCache()
                Log.d(logTag, "Retrieved item from cache: $key -> ${item.value}")
                item.value
            } else {
                cache.remove(item)
                saveCache()
                Log.d(logTag, "Item expired and removed from cache: $key")
                null
            }
        } else {
            Log.d(logTag, "Item not found in cache for key: $key")
            null
        }
    }

    private fun saveCache() {
        try {
            val cacheJson = gson.toJson(cache)
            sharedPreferences.edit().putString("cache", cacheJson).apply()
            Log.d(logTag, "Saved cache to SharedPreferences")
        } catch (e: Exception) {
            Log.e(logTag, "Failed to save cache to SharedPreferences", e)
        }
    }
}
