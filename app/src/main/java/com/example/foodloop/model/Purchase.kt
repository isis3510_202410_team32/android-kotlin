package com.example.foodloop.model

data class Purchase (
    val discount: Float =0F,
    val purchaseDate: String = "",
    val userId: String = "",
){
}