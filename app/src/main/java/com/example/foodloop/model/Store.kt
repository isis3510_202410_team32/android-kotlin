package com.example.foodloop.model

data class Store(
    val address: String = "",
    val imageUrl: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val name: String = "",
    val distance: Double = 0.0,
    val id: String = ""
)