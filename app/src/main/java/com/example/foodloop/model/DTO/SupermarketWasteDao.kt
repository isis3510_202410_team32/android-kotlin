package com.example.foodloop.domain

import android.util.Log
import com.example.foodloop.model.SupermarketWaste
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.toObject

class SupermarketWasteDao {

    private val db = FirebaseFirestore.getInstance()
    private val supermarketsCollection = db.collection("supermarketsWaste")

    fun getSupermarkets(callback: (List<SupermarketWaste>?, Exception?) -> Unit) {
        supermarketsCollection.get()
            .addOnSuccessListener { result ->
                val supermarketsWasteList = mutableListOf<SupermarketWaste>()
                for (document in result) {
                    val supermarket = document.toObject<SupermarketWaste>().copy(id = document.id)
                    supermarketsWasteList.add(supermarket)
                }
                callback(supermarketsWasteList, null)
                Log.d("SupermarketWasteDao", "Data loaded successfully")
            }
            .addOnFailureListener { exception ->
                callback(null, exception)
                Log.e("SupermarketWasteDao", "Error loading data", exception)
            }
    }
}
