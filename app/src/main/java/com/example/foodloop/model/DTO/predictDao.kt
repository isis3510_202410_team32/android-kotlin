package com.example.foodloop.domain

import com.example.foodloop.model.Predict
import com.google.firebase.firestore.FirebaseFirestore

class PredictDao {
    private val db = FirebaseFirestore.getInstance()
    private val predictionsCollection = db.collection("predictions")

    fun getPredictions(callback: (List<Predict>?, Exception?) -> Unit) {
        predictionsCollection.get()
            .addOnSuccessListener { result ->
                val predicts = mutableListOf<Predict>()
                for (document in result.documents) {
                    try {

                        val predict = document.toObject(Predict::class.java)?.apply {
                            id = document.id
                        }
                        predict?.let { predicts.add(it) }
                    } catch (e: Exception) {
                        callback(null, Exception("Error converting Firestore document to Predict", e))
                        return@addOnSuccessListener
                    }
                }
                callback(predicts, null)
            }
            .addOnFailureListener { exception ->
                callback(null, exception)
            }
    }
}
