package com.example.foodloop.domain

import com.example.foodloop.model.Supermarket
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.toObject

class SupermarketDao {

    private val db = FirebaseFirestore.getInstance()
    private val supermarketsCollection = db.collection("supermarkets")

    fun getSupermarkets(callback: (List<Supermarket>?, Exception?) -> Unit) {
        supermarketsCollection.get()
            .addOnSuccessListener { result ->
                val supermarkets = mutableListOf<Supermarket>()
                for (document in result) {
                    val supermarket = document.toObject<Supermarket>().copy(id = document.id)
                    supermarkets.add(supermarket)
                }
                callback(supermarkets, null)
            }
            .addOnFailureListener { exception ->
                callback(null, exception)
            }
    }
}
