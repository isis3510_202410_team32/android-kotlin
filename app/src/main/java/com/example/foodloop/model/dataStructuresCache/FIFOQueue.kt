package com.example.foodloop.ui

import android.content.Context
import android.util.Log
import com.example.foodloop.model.Product
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class FIFOQueue(private val context: Context, private val maxSize: Int) {
    private val queue = ArrayDeque<Pair<String, List<Product>>>()
    private val logTag = "CacheFIFOQueue"
    private val sharedPreferences = context.getSharedPreferences("FIFOQueue", Context.MODE_PRIVATE)
    private val gson = Gson()

    init {
        loadQueue()
    }

    private fun loadQueue() {
        val queueJson = sharedPreferences.getString("queue", null)
        if (queueJson != null) {
            try {
                val type = object : TypeToken<ArrayDeque<Pair<String, List<Product>>>>() {}.type
                val loadedQueue: ArrayDeque<Pair<String, List<Product>>> = gson.fromJson(queueJson, type)
                queue.clear()
                queue.addAll(loadedQueue)
            } catch (e: Exception) {
                Log.e(logTag, "Failed to load queue from SharedPreferences", e)
            }
        }
    }

    fun put(key: String, value: List<Product>?) {
        if (value == null) {
            Log.d(logTag, "Attempted to add null value for key: $key")
            return
        }

        if (queue.any { it.first == key }) {
            queue.removeIf { it.first == key }
            Log.d(logTag, "Updated existing item in cache: $key -> $value")
        } else if (queue.size >= maxSize) {
            val removed = queue.removeFirst()
            Log.d(logTag, "Removed oldest item from cache: ${removed.first} -> ${removed.second}")
        }
        queue.addLast(Pair(key, value))
        saveQueue()
        Log.d(logTag, "Added item to cache: $key -> $value")
    }


    fun get(key: String): List<Product>? {
        val item = queue.find { it.first == key }
        if (item != null) {
            queue.remove(item)
            queue.addLast(item)
            saveQueue()
            Log.d(logTag, "Retrieved item from cache: $key -> ${item.second}")
            return item.second
        } else {
            Log.d(logTag, "Item not found in cache for key: $key")
            return null
        }
    }

    private fun saveQueue() {
        val queueJson = gson.toJson(queue)
        sharedPreferences.edit().putString("queue", queueJson).apply()
    }
}
