package com.example.foodloop.model

data class Supermarket(
    val id: String = "",
    val address: String = "",
    val imageUrl: String = "",
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val name: String = "",
)


