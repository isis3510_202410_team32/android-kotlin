package com.example.foodloop.model.service


interface StorageService {
     fun getChartData(onError: (Throwable) -> Unit, onSuccess: (Array<Float>) -> Unit)
}