package com.example.foodloop.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foodloop.database.repositories.MealRepository
import com.example.foodloop.model.DTO.Meal
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MealViewModel() : ViewModel() {

    private val repository: MealRepository = MealRepository()

    private val emptyList: List<Meal> = emptyList()
    private val _meals = MutableStateFlow<List<Meal>?>(emptyList)
    val meals: StateFlow<List<Meal>?> get() = _meals

    fun fetchMeals(ingredient: String) {
        viewModelScope.launch (Dispatchers.IO) {
            val meals = repository.getMealsByIngredient(ingredient)
            _meals.value = meals
        }
    }
}
