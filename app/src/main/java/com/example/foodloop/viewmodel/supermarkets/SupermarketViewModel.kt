package com.example.foodloop.viewmodel.supermarkets

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foodloop.domain.service.StorageService
import com.example.foodloop.domain.service.implementation.APIStorageService
import com.example.foodloop.model.Store
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class SupermarketViewModel : ViewModel() {
    private val _supermarketsData = MutableStateFlow<Array<Store>>(emptyArray())
    val supermarketsData: StateFlow<Array<Store>> = _supermarketsData

    private val storageService : StorageService = APIStorageService()

    fun fetchSupermarketsData(latitude: Double, longitude: Double){
        viewModelScope.launch (Dispatchers.IO) {
            storageService.getSupermarkets(
                onSuccess = {
                    val results: Array<Store> = it
                    _supermarketsData.value = results
                    Log.d("EXITO!", it.toString())
                },
                onError = {
                    _supermarketsData.value = arrayOf()
                    Log.d("ERROR", it.message.toString())
                },
                latitude = latitude,
                longitude = longitude,
                radius = 20000.0
            )
        }
    }
}