package com.example.foodloop.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class WasteViewModelFactory(private val application: Application) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(WasteViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return WasteViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
