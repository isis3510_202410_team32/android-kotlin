package com.example.foodloop.viewmodel.orders

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foodloop.domain.Order
import com.example.foodloop.domain.service.StorageService
import com.example.foodloop.domain.service.implementation.APIStorageService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class OrderListViewModel  : ViewModel() {
    private val _ordersData = MutableStateFlow<Array<Order>>(emptyArray())
    val ordersData: StateFlow<Array<Order>> = _ordersData

    private val storageService : StorageService = APIStorageService()

    fun fetchOrdersData(userId:String){
        viewModelScope.launch (Dispatchers.IO) {
            storageService.getPurchasesByUser (
                onSuccess = {
                    val results: Array<Order> = it
                    _ordersData.value = results
                    Log.d("EXITO!", it.toString())
                },
                onError = {
                    _ordersData.value = arrayOf()
                    Log.d("ERROR", it.message.toString())
                },
                userId = userId
            )
        }
    }
}