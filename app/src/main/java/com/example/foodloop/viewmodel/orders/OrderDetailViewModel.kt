package com.example.foodloop.viewmodel.orders

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foodloop.domain.Order
import com.example.foodloop.domain.service.StorageService
import com.example.foodloop.domain.service.implementation.FirebaseStorageService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class OrderDetailViewModel : ViewModel()  {
    private val _orderData = MutableStateFlow<Order?>(Order())
    val orderData: StateFlow<Order?> = _orderData

    private val storageService : StorageService = FirebaseStorageService()

    fun fetchOrderData(id: String){
        viewModelScope.launch (Dispatchers.IO) {
            storageService.getPurchase(
                onSuccess = {
                    val result: Order? = it
                    _orderData.value = result
                    Log.d("EXITO!", it.toString())
                },
                onError = {
                    _orderData.value = null
                    Log.d("ERROR", it.message.toString())
                },
                orderId = id
            )
        }
    }
}