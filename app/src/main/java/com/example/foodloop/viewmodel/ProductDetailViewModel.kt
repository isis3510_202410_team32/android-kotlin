package com.example.foodloop.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.foodloop.database.repositories.DBRepository

class ProductDetailViewModel(repository: DBRepository): ViewModel() {
    private val _productId = MutableLiveData<String>()

    fun setInitialProduct(productId: String){
        _productId.value = productId
    }
}

class ProductViewModelFactory(private val repository: DBRepository): ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ProductDetailViewModel::class.java)){
            @Suppress("UNCHECKED_CAST")
            return ProductDetailViewModel(repository) as T
        }
        throw IllegalArgumentException ("Unkonow ViewModel Class")
    }
}