package com.example.foodloop.viewmodel.savings

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foodloop.domain.service.StorageService
import com.example.foodloop.domain.service.implementation.APIStorageService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class SavingsViewModel : ViewModel() {
    private val _savingsData = MutableStateFlow<Array<Float>>(emptyArray())
    val savingsData: StateFlow<Array<Float>> = _savingsData

    private val storageService: StorageService = APIStorageService()

    fun fetchSavingsData(userId: String, year: Int){
        viewModelScope.launch (Dispatchers.IO) {
            storageService.getSavingsDataByUser(
                onSuccess = {
                    val results: Array<Float> = it
                    _savingsData.value = results
                    Log.d("EXITO!", it.toString())
                },
                onError = {
                    _savingsData.value = Array(12) { 0F }
                    Log.d("ERROR", it.message.toString())
                },
                userId = userId,
                year = year
            )
        }
    }
}
