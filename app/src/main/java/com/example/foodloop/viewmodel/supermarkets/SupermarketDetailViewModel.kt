package com.example.foodloop.viewmodel.supermarkets

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.foodloop.domain.service.StorageService
import com.example.foodloop.domain.service.implementation.FirebaseStorageService
import com.example.foodloop.model.Store
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class SupermarketDetailViewModel : ViewModel() {
    private val _supermarketData = MutableStateFlow<Store?>(Store())
    val supermarketData: StateFlow<Store?> = _supermarketData

    private val storageService : StorageService = FirebaseStorageService()

    fun fetchSupermarketData(id: String){
        viewModelScope.launch (Dispatchers.IO) {
            storageService.getSupermarket(
                onSuccess = {
                    val result: Store? = it
                    _supermarketData.value = result
                    Log.d("EXITO!", it.toString())
                },
                onError = {
                    _supermarketData.value = null
                    Log.d("ERROR", it.message.toString())
                },
                id = id
            )
        }
    }
}