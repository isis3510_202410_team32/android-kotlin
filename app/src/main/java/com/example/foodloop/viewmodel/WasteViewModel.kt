package com.example.foodloop.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.foodloop.database.DatabaseHelper
import com.example.foodloop.domain.SupermarketWasteDao
import com.example.foodloop.model.LocalFoodWaste
import com.example.foodloop.model.SupermarketWaste
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class WasteViewModel(application: Application) : AndroidViewModel(application) {

    private val _supermarketWastes = MutableLiveData<List<SupermarketWaste>?>()
    val supermarketWastes: LiveData<List<SupermarketWaste>?> get() = _supermarketWastes

    private val _localFoodWastes = MutableLiveData<List<LocalFoodWaste>>()
    val localFoodWastes: LiveData<List<LocalFoodWaste>> get() = _localFoodWastes

    private val supermarketWasteDao = SupermarketWasteDao()
    private val databaseHelper: DatabaseHelper = DatabaseHelper(application)

    init {
        loadSupermarketWastes()
        loadLocalFoodWastes()
    }

    fun loadSupermarketWastes() {
        viewModelScope.launch {
            try {
                supermarketWasteDao.getSupermarkets { supermarkets, exception ->
                    if (exception == null) {
                        _supermarketWastes.postValue(supermarkets ?: emptyList())
                    } else {
                        _supermarketWastes.postValue(emptyList())
                    }
                }
            } catch (e: Exception) {
                _supermarketWastes.postValue(emptyList())
            }
        }
    }

    fun loadLocalFoodWastes() {
        viewModelScope.launch(Dispatchers.IO) {
            val wastes = databaseHelper.getAllLocalFoodWaste()
            withContext(Dispatchers.Main) {
                _localFoodWastes.value = wastes
            }
        }
    }

    fun insertLocalFoodWaste(name: String, foodType: String, weight: Double) {
        viewModelScope.launch(Dispatchers.IO) {
            databaseHelper.insertLocalFoodWaste(name, foodType, weight)
            loadLocalFoodWastes()
        }
    }
}
