package com.example.foodloop.viewmodel

import android.util.Log
import androidx.lifecycle.viewModelScope
import com.example.foodloop.model.Predict
import com.example.foodloop.domain.PredictDao
import com.example.foodloop.domain.ProductDao
import com.example.foodloop.model.Product

import com.example.foodloop.model.Supermarket
import com.example.foodloop.domain.SupermarketDao
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.coroutines.CompletableDeferred
import com.example.foodloop.ui.ExpiryCache
import com.example.foodloop.ui.LRUCache
import com.example.foodloop.ui.FIFOQueue
import android.app.Application
import androidx.lifecycle.AndroidViewModel

class HomePageViewModel(
    application: Application,
    private val supermarketDao: SupermarketDao,
    private val productDao: ProductDao,
    private val predictDao: PredictDao
) : AndroidViewModel(application) {

    private val supermarketCache = LRUCache<String, List<Supermarket>>(100000)
    val productCache = FIFOQueue(getApplication<Application>().applicationContext, 10)
    private val predictionCache = ExpiryCache(getApplication<Application>().applicationContext, 10)

    private val _supermarkets = MutableStateFlow<List<Supermarket>?>(null)
    val supermarkets: StateFlow<List<Supermarket>?> = _supermarkets

    private val _products = MutableStateFlow<List<Product>?>(null)
    val products: StateFlow<List<Product>?> = _products

    private val _predictions = MutableStateFlow<List<Predict>?>(null)
    val predictions: StateFlow<List<Predict>?> = _predictions

    init {
        loadDataIfNeeded()
    }

    private fun loadDataIfNeeded() {
        if (_supermarkets.value.isNullOrEmpty()) {
            loadSupermarkets()
        }
        if (_products.value.isNullOrEmpty()) {
            loadProducts()
        }
        if (_predictions.value.isNullOrEmpty()) {
            loadPredictions()
        }
    }

    private fun loadSupermarkets() {
        viewModelScope.launch {
            val key = "supermarkets"
            val cachedData = supermarketCache.get(key)
            if (cachedData != null) {
                _supermarkets.value = cachedData
            } else {
                try {
                    val (supermarkets, exception) = fetchAsync { callback ->
                        supermarketDao.getSupermarkets(callback)
                    }
                    if (exception == null) {
                        supermarketCache.put(key, supermarkets ?: emptyList())
                        _supermarkets.value = supermarkets ?: emptyList()
                    } else {
                        Log.e("HomePageViewModel", "Error fetching supermarkets: ${exception.message}")
                        _supermarkets.value = emptyList()
                    }
                } catch (e: Exception) {
                    Log.e("HomePageViewModel", "Exception loading supermarkets: ${e.message}")
                    _supermarkets.value = emptyList()
                }
            }
        }
    }


    private fun loadProducts() {
        viewModelScope.launch {
            val key = "products"
            val cachedData = productCache.get(key)
            if (cachedData != null) {
                _products.value = cachedData
                Log.d("HomePageViewModelFIFO", "Loaded products from cache")
            } else {
                productDao.getProducts { products, exception ->
                    if (exception == null && products != null) {
                        productCache.put(key, products)
                        _products.value = products
                        Log.d("HomePageViewModelFIFO", "Fetched products from database and updated cache")
                    } else if (exception != null) {
                        Log.e("HomePageViewModel", "Error fetching products: ${exception.message}")
                        _products.value = emptyList()
                    } else {
                        _products.value = emptyList()
                    }
                }
            }
        }
    }



    private fun loadPredictions() {
        viewModelScope.launch {
            val key = "predictions"
            val cachedData = predictionCache.get(key)
            if (cachedData != null) {
                _predictions.value = cachedData
            } else {
                try {
                    val (predictions, exception) = fetchAsync { callback ->
                        predictDao.getPredictions(callback)
                    }
                    if (exception == null) {
                        predictionCache.put(key, predictions ?: emptyList())
                        _predictions.value = predictions ?: emptyList()
                    } else {
                        Log.e("HomePageViewModel", "Error fetching predictions: ${exception.message}")
                        _predictions.value = emptyList()
                    }
                } catch (e: Exception) {
                    Log.e("HomePageViewModel", "Exception loading predictions: ${e.message}")
                    _predictions.value = emptyList()
                }
            }
        }
    }

    private suspend fun <T> fetchAsync(fetchFunction: (callback: (T?, Exception?) -> Unit) -> Unit): Pair<T?, Exception?> {
        return withContext(Dispatchers.IO) {
            val result = CompletableDeferred<Pair<T?, Exception?>>()
            fetchFunction { data, exception ->
                result.complete(data to exception)
            }
            result.await()
        }
    }
}