package com.example.foodloop.viewmodel

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.foodloop.domain.PredictDao
import com.example.foodloop.domain.ProductDao
import com.example.foodloop.domain.SupermarketDao

class HomePageViewModelFactory(
    private val application: Application,
    private val supermarketDao: SupermarketDao,
    private val productDao: ProductDao,
    private val predictDao: PredictDao
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomePageViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return HomePageViewModel(application, supermarketDao, productDao, predictDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}