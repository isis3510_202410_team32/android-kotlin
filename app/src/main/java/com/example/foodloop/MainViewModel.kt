package com.example.foodloop

import android.util.Log
import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.delay
import androidx.lifecycle.viewModelScope
//import com.example.foodloop.ui.Month

@OptIn(FlowPreview::class)
class MainViewModel: ViewModel() {


    private val _searchText = MutableStateFlow("")
    val searchText = _searchText.asStateFlow()

    private val _isSearching = MutableStateFlow(false)
    val isSearching = _isSearching.asStateFlow()

    private val _products = MutableStateFlow(allProducts)
    val products = searchText
        .debounce(1000L)
        .onEach { _isSearching.update { true } }
        .combine(_products) { text, products ->
            if(text.isBlank()) {
                products
            } else {
                delay(1000L)
                products.filter {
                    it.doesMatchSearchQuery(text)
                }
                //Log.d("SearchScreen", "Products: $products")
            }
        }
        .onEach { _isSearching.update { false } }
        .stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(5000),
            _products.value
        )

    /*val productName = searchText
        .debounce(1000L)
        .onEach { _isSearching.value = true }
        .transformLatest { text ->
            if (text.isBlank()) {
                emit(allProducts)
            } else {
                delay(2000L)
                val filteredProducts = allProducts.filter { it.doesMatchSearchQuery(text) }
                emit(filteredProducts)
            }
        }
        .onEach { _isSearching.value = false }
        .stateIn(
            viewModelScope,
            SharingStarted.WhileSubscribed(5000),
            allProducts
        )*/


    fun onSearchTextChange(text: String) {
        _searchText.value = text
    }
}


data class Product(
    val productName: String,
){
    fun doesMatchSearchQuery(query: String): Boolean {
        return productName.contains(query, ignoreCase = true)
    }
}

private val allProducts = listOf(
    Product(
        productName = "Agua sin gas"
    ),
    Product(
        productName = "Agua con gas"
    ),

    Product(
        productName = "Aguacate"
    ),
    Product(
        productName = "Galletas ducales"
    )
)