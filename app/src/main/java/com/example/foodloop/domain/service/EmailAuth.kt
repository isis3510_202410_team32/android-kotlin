package com.example.foodloop.domain.service

import android.content.Context
import android.widget.Toast
import com.example.foodloop.domain.service.AuthStrategy
import com.google.firebase.auth.FirebaseAuth

class EmailAuthStrategy(private val context: Context) : AuthStrategy {

    private var auth: FirebaseAuth = FirebaseAuth.getInstance()

    override fun login(email: String, password: String, onResult: (Boolean, String?) -> Unit) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val userId = auth.currentUser?.uid
                    Toast.makeText(context, "Authentication Successful. $userId", Toast.LENGTH_SHORT).show()
                    onResult(true, userId)
                } else {
                    Toast.makeText(context, "Authentication Failed.", Toast.LENGTH_SHORT).show()
                    onResult(false, null)
                }
            }
    }
    override fun register(
        email: String,
        password: String,
        confirmPassword: String,
        onResult: (Boolean, String?) -> Unit
    ) {
        if (password != confirmPassword) {
            Toast.makeText(context, "Passwords do not match.", Toast.LENGTH_SHORT).show()
            onResult(false,null)
            return
        }
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val userId = auth.currentUser?.uid

                    Toast.makeText(
                        context,
                        "Registration Successful. $userId",
                        Toast.LENGTH_SHORT
                    ).show()
                    onResult(true, userId)
                } else {
                    Toast.makeText(context, "Registration Failed.", Toast.LENGTH_SHORT).show()
                    onResult(false,null)
                }
            }
    }
}
