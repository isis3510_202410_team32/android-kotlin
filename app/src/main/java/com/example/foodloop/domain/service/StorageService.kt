package com.example.foodloop.domain.service

import com.example.foodloop.domain.Order
import com.example.foodloop.model.Store

interface StorageService {
     fun getSavingsDataByUser(onError: (Throwable) -> Unit, onSuccess: (Array<Float>) -> Unit, userId: String, year: Int )
     fun getSupermarkets(onError: (Throwable) -> Unit, onSuccess: (Array<Store>) -> Unit, latitude: Double, longitude: Double, radius: Double )
     fun getSupermarket(onError: (Throwable) -> Unit, onSuccess: (Store?) -> Unit, id: String )
     fun getPurchasesByUser(onError: (Throwable) -> Unit, onSuccess: (Array<Order>) -> Unit, userId: String)
     fun getPurchase(onError: (Throwable) -> Unit, onSuccess: (Order?) -> Unit, orderId: String)
}