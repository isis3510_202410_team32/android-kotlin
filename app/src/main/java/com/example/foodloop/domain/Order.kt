package com.example.foodloop.domain

data class Order (
    val address:String = "",
    val cardNumber:String = "",
    val deliveryType:String = "",
    val discount:Double = 0.0,
    val paymentMethod:String = "",
    val purchaseDate:String = "",
    val subtotal:Double = 0.0,
    val totalPrice:Double = 0.0,
    val userId:String ="",
    var id:String = "",
    val products: List<Product> = listOf()
)