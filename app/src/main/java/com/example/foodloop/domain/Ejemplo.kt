import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener

fun main() {
    val database = FirebaseDatabase.getInstance()
    val databaseReference = database.getReference("users")

    val email = "andrekspro13@gmail.com"
    databaseReference.orderByChild("email").equalTo(email).addListenerForSingleValueEvent(object : ValueEventListener {
        override fun onDataChange(dataSnapshot: DataSnapshot) {
            if (dataSnapshot.exists()) {
                for (snapshot in dataSnapshot.children) {
                    val user = snapshot.getValue(User::class.java)
                    println("Nombre: ${user?.name}")
                }
            } else {
                println("No se encontró un usuario con el correo electrónico: $email")
            }
        }

        override fun onCancelled(databaseError: DatabaseError) {
            println("Error al obtener datos: ${databaseError.message}")
        }
    })
}


