package com.example.foodloop.domain.service.implementation

import android.util.Log
import com.example.foodloop.domain.Order
import com.example.foodloop.domain.Purchase
import com.example.foodloop.domain.service.StorageService
import com.example.foodloop.model.Store
import com.google.firebase.Firebase
import com.google.firebase.database.database
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlin.math.PI
import kotlin.math.sin
import kotlin.math.cos
import kotlin.math.atan2
import kotlin.math.pow
import kotlin.math.sqrt

class FirebaseStorageService : StorageService {
    override fun getSavingsDataByUser(onError: (Throwable) -> Unit, onSuccess: (Array<Float>) -> Unit, userId: String, year: Int){
        val db = FirebaseFirestore.getInstance()

        var data = Array(12) { 0F }

        db.collection("purchase")
            .whereEqualTo("userId", userId)
            .get()
            .addOnSuccessListener {
                    result ->
                val purchasesData = Array(12){0F}

                for(document in result){
                    val purchase = document.toObject(Purchase::class.java)
                    val purchaseYear = purchase.purchaseDate.split("/")[2].toInt()
                    if(purchaseYear == year) {
                        val month = purchase.purchaseDate.split("/")[1].toInt()
                        purchasesData[month - 1] += purchase.discount
                    }
                }
                onSuccess(purchasesData)
            }
            .addOnFailureListener {
                Log.d("Firestore", "Can't get savings data information")
            }
    }

    override fun getSupermarkets(onError: (Throwable) -> Unit, onSuccess: (Array<Store>) -> Unit, latitude: Double, longitude: Double, radius: Double){
        val db = FirebaseFirestore.getInstance()
        var supermarketsData: Array<Store> = arrayOf()

        db.collection("supermarkets")
            .get()
            .addOnSuccessListener {
                    result ->

                for(document in result){
                    var supermarket = document.toObject(Store::class.java)
                    val distance = getDistance(latitude, longitude, supermarket.latitude, supermarket.longitude)
                    if(distance < radius) {
                        supermarketsData.plus(supermarket)
                    }
                }
                onSuccess(supermarketsData)
            }
            .addOnFailureListener {
                Log.d("Firestore", "Can't get supermarkets information")
            }
    }

    private fun getDistance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double{
        val earthRadius = 6371.0

        val latitude1 = lat1 * PI / 180.0
        val longitude1 = lon1 * PI / 180.0
        val latitude2 = lat2 * PI / 180.0
        val longitude2 = lon2 * PI / 180.0

        val dlat = latitude2 - latitude1
        val dlon = longitude2 - longitude1

        val a = sin(dlat / 2).pow(2) + cos(lat1) * cos(lat2) * sin(dlon / 2).pow(2)
        val c = 2 * atan2(sqrt(a), sqrt(1 - a))

        return earthRadius * c
    }

    override fun getSupermarket(onError: (Throwable) -> Unit, onSuccess: (Store?) -> Unit, id: String) {
        val db = FirebaseFirestore.getInstance()
        Firebase.database.setPersistenceEnabled(true)

        db.collection("supermarkets")
            .document(id)
            .get()
            .addOnSuccessListener {
                    result ->
                if(result.exists()){
                    val store = result.toObject(Store::class.java)
                    onSuccess(store)
                } else {
                    onSuccess(null)
                }
            }
            .addOnFailureListener {
                Log.d("Firestore", "Can't get supermarket information")
            }
    }


    override fun getPurchasesByUser(onError: (Throwable) -> Unit, onSuccess: (Array<Order>) -> Unit, userId: String) {

    }

    override fun getPurchase(onError: (Throwable) -> Unit, onSuccess: (Order?) -> Unit, orderId: String) {
        val db = FirebaseFirestore.getInstance()
        Firebase.database.setPersistenceEnabled(true)

        db.collection("purchases")
            .document(orderId)
            .get()
            .addOnSuccessListener {
                    result ->
                if(result.exists()){
                    val order = result.toObject(Order::class.java)
                    order?.id = orderId
                    onSuccess(order)
                } else {
                    onSuccess(null)
                }
            }
            .addOnFailureListener {
                Log.d("Firestore", "Can't get order information")
            }
    }
}