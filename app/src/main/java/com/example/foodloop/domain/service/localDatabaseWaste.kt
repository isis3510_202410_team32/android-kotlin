package com.example.foodloop.domain

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

class localDatabaseWaste(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "FoodWasteDatabase.db"
        private const val DATABASE_VERSION = 1
        private const val TABLE_FOOD_WASTE = "food_waste"
        private const val COLUMN_ID = "id"
        private const val COLUMN_NAME = "name"
        private const val COLUMN_FOOD_TYPE = "food_type"
        private const val COLUMN_WEIGHT = "weight"

        private const val SQL_CREATE_ENTRIES =
            """
            CREATE TABLE IF NOT EXISTS $TABLE_FOOD_WASTE (
                $COLUMN_ID INTEGER PRIMARY KEY AUTOINCREMENT,
                $COLUMN_NAME TEXT NOT NULL,
                $COLUMN_FOOD_TYPE TEXT NOT NULL,
                $COLUMN_WEIGHT REAL NOT NULL
            );
            """

        private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS $TABLE_FOOD_WASTE"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL(SQL_DELETE_ENTRIES)
        onCreate(db)
    }

    fun addFoodWaste(name: String, foodType: String, weight: Double): Boolean {
        val db = writableDatabase
        val values = ContentValues().apply {
            put(COLUMN_NAME, name)
            put(COLUMN_FOOD_TYPE, foodType)
            put(COLUMN_WEIGHT, weight)
        }

        val newRowId = db.insert(TABLE_FOOD_WASTE, null, values)
        if (newRowId == -1L) {
            Log.e("DatabaseHelper", "Failed to add food waste")
        } else {
            Log.i("DatabaseHelper", "Food waste added successfully")
        }
        return newRowId != -1L
    }

    fun getAllFoodWaste(): List<FoodWaste> {
        val foodWasteList = mutableListOf<FoodWaste>()
        val db = readableDatabase
        val cursor = db.query(
            TABLE_FOOD_WASTE,
            arrayOf(COLUMN_ID, COLUMN_NAME, COLUMN_FOOD_TYPE, COLUMN_WEIGHT),
            null,
            null,
            null,
            null,
            null
        )
        cursor.use {
            while (it.moveToNext()) {
                val id = it.getInt(it.getColumnIndexOrThrow(COLUMN_ID))
                val name = it.getString(it.getColumnIndexOrThrow(COLUMN_NAME))
                val foodType = it.getString(it.getColumnIndexOrThrow(COLUMN_FOOD_TYPE))
                val weight = it.getDouble(it.getColumnIndexOrThrow(COLUMN_WEIGHT))
                foodWasteList.add(FoodWaste(id, name, foodType, weight))
            }
        }
        return foodWasteList
    }
}

data class FoodWaste(
    val id: Int,
    val name: String,
    val foodType: String,
    val weight: Double
)
