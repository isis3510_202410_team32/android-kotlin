package com.example.foodloop.domain

class Product (
    val description: String = "",
    val discountPrice: Double = 0.0,
    val expirationDate: String = "",
    val imageUrl: String = "",
    val name: String = "",
    val price: Double = 0.0,
    val id: String = ""
)