package com.example.foodloop.domain

import android.provider.ContactsContract.CommonDataKinds.Email

data class Supermarket(
    val id: String,
    val name: String,
    val address: String,
    val imageUrl: String,
    val latitude: Double,
    val longitude: Double
)

data class Product1(
    val id: String,
    val name: String,
    val description: String,
    val price: Int,
    val discountPrice: Int,
    val imageUrl: String
)

data class User(
    val id: String,
    val email: String,
    val name: String,
    val password: String
)

data class Favorite(
    val userId: String,
    val productId: String
)

