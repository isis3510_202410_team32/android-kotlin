package com.example.foodloop.domain.service

interface AuthStrategy {
    fun login(email: String, password: String, onResult: (Boolean, String?) -> Unit)
    fun register(
        email: String,
        password: String,
        confirmPassword: String,
        onResult: (Boolean,String?) -> Unit
    )
}
