package com.example.foodloop.domain

data class Purchase (
    val discount: Float =0F,
    val purchaseDate: String = "",
    val userId: String = "",
)
