package com.example.foodloop.model

data class LocalFoodWaste(
    val id: Int = 0,
    val name: String,
    val foodType: String,
    val weight: Double
)
