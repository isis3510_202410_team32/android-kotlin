package com.example.foodloop.domain

import android.content.Context
import com.google.firebase.firestore.FirebaseFirestore

class UserDao(context: Context) {
    private val dbFirebase = FirebaseFirestore.getInstance()
    private val usersCollection = dbFirebase.collection("users")
    private val dbHelper = DatabaseHelper(context)

    fun createUser(user: User, callback: (Boolean) -> Unit) {
        usersCollection.document(user.id).set(user)
            .addOnSuccessListener {
                addLocalUser(user) { success ->
                    callback(success)
                }
            }
            .addOnFailureListener {
                callback(false)
            }
    }
    private fun addLocalUser(user: User, callback: (Boolean) -> Unit) {
        val result = dbHelper.addUser(user.id, user.name, user.email, user.password)
        callback(result)
    }
}
