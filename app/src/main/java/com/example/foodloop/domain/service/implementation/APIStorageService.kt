package com.example.foodloop.domain.service.implementation

import android.util.Log
import com.example.foodloop.domain.Order
import com.example.foodloop.domain.service.StorageService
import com.example.foodloop.model.Store
import com.google.gson.Gson
import com.google.gson.JsonObject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class APIStorageService : StorageService {

    override fun getSavingsDataByUser(onError: (Throwable) -> Unit, onSuccess: (Array<Float>) -> Unit, userId: String, year: Int){
        val jsonObject = JsonObject()
        jsonObject.addProperty("user", userId)
        jsonObject.addProperty("year", year)

        asyncGetHttpRequest(
            endpoint = "https://get-discount-data-by-user-c5ntct2jrq-uw.a.run.app/",
            params = jsonObject,
            onSuccess = {
                val results: Array<Float> = Gson().fromJson(it.response, Array<Float>::class.java)
                onSuccess(results)
            },
            onError = {
                Log.d("ERROR", it.message.toString())
            }
        )
    }

    override fun getSupermarkets(onError: (Throwable) -> Unit, onSuccess: (Array<Store>) -> Unit, latitude: Double, longitude: Double, radius: Double) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("latitude", latitude)
        jsonObject.addProperty("longitude", longitude)
        jsonObject.addProperty("radius", 20000.0F)

        asyncGetHttpRequest(
            endpoint = "https://get-nearest-supermarkets-c5ntct2jrq-uw.a.run.app/",
            params = jsonObject,
            onSuccess = {
                val results: Array<Store> = Gson().fromJson(it.response, Array<Store>::class.java)
                onSuccess(results)
            },
            onError = {
                Log.d("ERROR", it.message.toString())
            }
        )
    }

    override fun getSupermarket(onError: (Throwable) -> Unit, onSuccess: (Store?) -> Unit, id: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("id", id)

        asyncGetHttpRequest(
            endpoint = "https://get-supermarket-c5ntct2jrq-uw.a.run.app/",
            params = jsonObject,
            onSuccess = {
                val prevResult:JsonObject = Gson().fromJson(it.response, JsonObject::class.java)

                if(prevResult.entrySet().isEmpty())
                    onSuccess(null)
                else {
                    val result = Gson().fromJson(it.response, Store::class.java)
                    onSuccess(result)
                }
            },
            onError = {
                Log.d("ERROR", it.message.toString())
            }
        )
    }

    override fun getPurchasesByUser(onError: (Throwable) -> Unit, onSuccess: (Array<Order>) -> Unit, userId: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("user", userId)

        asyncGetHttpRequest(
            endpoint = "https://get-purchases-by-user-c5ntct2jrq-uw.a.run.app",
            params = jsonObject,
            onSuccess = {
                val result = Gson().fromJson(it.response, Array<Order>::class.java)
                onSuccess(result)
            },
            onError = {
                Log.d("ERROR", it.message.toString())
            }
        )
    }

    override fun getPurchase(onError: (Throwable) -> Unit, onSuccess: (Order?) -> Unit, orderId: String) {
        val jsonObject = JsonObject()
        jsonObject.addProperty("id", orderId)

        asyncGetHttpRequest(
            endpoint = "https://get-purchase-c5ntct2jrq-uw.a.run.app",
            params = jsonObject,
            onSuccess = {
                val prevResult:JsonObject = Gson().fromJson(it.response, JsonObject::class.java)

                if(prevResult.entrySet().isEmpty())
                    onSuccess(null)
                else {
                    val result = Gson().fromJson(it.response, Order::class.java)
                    onSuccess(result)
                }
            },
            onError = {
                Log.d("ERROR", it.message.toString())
            }
        )
    }

    private fun asyncGetHttpRequest(
        endpoint: String,
        params: JsonObject,
        onSuccess: (ApiResponse) -> Unit,
        onError: (Exception) -> Unit
    ) {
        CoroutineScope(Dispatchers.IO).launch {
            // Convert JSONObject to String
            val jsonObjectString = params.toString()

            val url = URL(endpoint)
            val openedConnection = url.openConnection() as HttpURLConnection
            openedConnection.requestMethod = "GET"
            openedConnection.setRequestProperty("Content-Type", "application/json") // The format of the content we're sending to the server
            openedConnection.setRequestProperty("Accept", "application/json") // The format of response we want to get from the server
            openedConnection.doInput = true
            openedConnection.doOutput = true

            val outputStreamWriter = OutputStreamWriter(openedConnection.outputStream)
            outputStreamWriter.write(jsonObjectString)
            outputStreamWriter.flush()

            val responseCode = openedConnection.responseCode
            try {
                val reader = BufferedReader(InputStreamReader(openedConnection.inputStream))
                val response = reader.readText()

                val apiResponse = ApiResponse(
                    responseCode,
                    response
                )
                print(response)
                reader.close()

                launch(Dispatchers.Main) {
                    onSuccess(apiResponse)
                }
            } catch (e: Exception) {
                Log.d("Error", e.message.toString())

                launch(Dispatchers.Main) {
                    onError(Exception("HTTP Request failed with response code $responseCode"))
                }
            } finally {

            }
        }
    }

    data class ApiResponse(
        val responseCode: Int,
        val response: String
    )
}