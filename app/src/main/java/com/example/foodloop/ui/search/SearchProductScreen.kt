package com.example.foodloop.ui.search

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.collectAsState
import com.example.foodloop.MainViewModel
import com.example.foodloop.R

@Composable
fun SearchProductScreen(
    viewModel: MainViewModel,
    onProductButtonClicked: () -> Unit,
    modifier: Modifier = Modifier,
    contentPadding: PaddingValues = PaddingValues(0.dp, 200.dp, 0.dp, 0.dp)
) {
    val products = viewModel.products.collectAsState()

    Column (
        modifier.fillMaxSize()
    ){
        SearchBar(
            viewModel,
            Modifier
                .padding(horizontal = 16.dp)
                .requiredHeight(64.dp)
        )
        Products(Modifier.fillMaxSize(), onClick = onProductButtonClicked)
    }
}

@Composable
fun Products(
    modifier: Modifier = Modifier,
    names: List<String> = List(10) { "Nombre producto ${it+1}" },
    onClick: () -> Unit
) {
    LazyColumn(
        contentPadding = PaddingValues(6.dp, 6.dp, 6.dp, 6.dp),
        modifier = modifier
            .padding(4.dp)
            .fillMaxSize()
    ) {
        items(items = names) { name ->
            Product(productName = name, onClick = onClick)
            Spacer(Modifier.height(6.dp))
        }
    }
}

@Composable
fun Product(productName: String, modifier: Modifier = Modifier, onClick: () -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .clickable(
                onClick = onClick
            )
            .background(Color(0xFFF5F5F5))
    ) {
        Image(
            painter = painterResource(id = R.drawable.margarita_natural),
            contentDescription = "image 60",
            modifier = Modifier
                .padding(8.dp)
                .clip(shape = RoundedCornerShape(8.dp))
        )
        /*AsyncImage(
            model = ImageRequest.Builder(context = LocalContext.current).data("https://lh3.googleusercontent.com/m65j11nlLTuZ86PadapPUcBUPTufpXyqXpwxdbYEZAIcuNYUTqrFpFElW-k_YuGKSCvjlaOvHWhsDJdMtdq7-rOYX-EpBgdi1A")
                .crossfade(true).build(),
            error = painterResource(R.drawable.ic_broken_image),
            placeholder = painterResource(R.drawable.loading_img),
            contentDescription = "",
            contentScale = ContentScale.Fit,
            modifier = Modifier.requiredWidth(64.dp)
        )*/
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = productName,
                color = Color.Black,
                style = TextStyle(
                    fontSize = 14.sp
                ),
                modifier = Modifier
                    .padding(8.dp)
            )
            Icon(
                imageVector = Icons.Default.Search,
                contentDescription = null,
                modifier = Modifier.padding(end = 8.dp)
            )
        }
    }
}