package com.example.foodloop.ui

import androidx.compose.ui.graphics.painter.Painter

data class Recipe1(
    val title: String,
    val difficulty: String,
    val calories: Int,
    val time: String,
    val imagePainter: Painter,
    val description: String? = null
)
