package com.example.foodloop.ui.orders

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.widget.Toast
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.foodloop.domain.Order
import com.example.foodloop.ui.theme.Green
import com.example.foodloop.viewmodel.orders.OrderListViewModel


@Composable
fun OrderListScreen(
    modifier: Modifier = Modifier,
    onOrderButtonClicked: (orderId: String) -> Unit,
    isSupportScreen:Boolean = false,
    contentPadding: PaddingValues = PaddingValues(0.dp, 200.dp, 0.dp, 0.dp)
){
    val orderListViewModel: OrderListViewModel = viewModel()
    val data by orderListViewModel.ordersData.collectAsState()
    val context = LocalContext.current
    val isConnected by remember { mutableStateOf(com.example.foodloop.ui.supermarkets.checkInternetConnection(context)) }

    if (isConnected) {
        LaunchedEffect(key1 = true) {
            orderListViewModel.fetchOrdersData("0cb485bb-8f41-431d-9f7a-d3e17217d4b9")
        }

        Column (
            modifier.fillMaxSize()
        ){
            UpperBarOrders(isSupportScreen = isSupportScreen)
            Orders(Modifier.fillMaxSize(), orders = data, onClick = onOrderButtonClicked)
        }
    } else {
        Toast.makeText(
            context,
            "You don't have an internet connection. Please check your connection and try again.",
            Toast.LENGTH_SHORT
        ).show()
    }
}

@Composable
fun Orders(
    modifier: Modifier = Modifier,
    //orders: List<Pair<String, String>> = List(10) { Pair("Order Number ${it + 1}", "Order Date ${it + 1}") },
    orders: Array<Order>,
    onClick: (orderId: String) -> Unit
) {
    LazyColumn(
        contentPadding = PaddingValues(6.dp, 6.dp, 6.dp, 6.dp),
        modifier = modifier
            .padding(4.dp)
            .fillMaxSize()
    ) {
        items(items = orders) { order ->
            Order(orderNumber = order.id, orderDate = order.purchaseDate, onClick = onClick)
            Spacer(Modifier.height(6.dp))
        }
    }
}

@Composable
fun Order(orderNumber:String, orderDate: String, modifier: Modifier = Modifier, onClick: (orderId: String) -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .clickable(
                onClick = { onClick(orderNumber) }
            )
            .background(Color(0xFFF5F5F5))
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Column(
                modifier = Modifier
                    .padding(8.dp)
            ) {
                Text(
                    text = "Order # $orderNumber",
                    color = Color.Black,
                    style = TextStyle(
                        fontSize = 18.sp
                    )
                )
                Spacer(Modifier.height(10.dp))
                Text(
                    text = orderDate,
                    color = Color.Gray,
                    style = TextStyle(
                        fontSize = 15.sp
                    )
                )
            }
            Icon(
                imageVector = Icons.Default.KeyboardArrowRight,
                contentDescription = null,
                modifier = Modifier.padding(end = 8.dp)
            )
        }
    }
}

fun checkInternetConnection(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val network = connectivityManager.activeNetwork
    val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
    return networkCapabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true
}

@Composable
fun UpperBarOrders(modifier: Modifier = Modifier, isSupportScreen: Boolean = false) {
    NavigationBar(
        containerColor = Green,
        modifier = modifier.requiredHeight(36.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            if(isSupportScreen){
                Text(
                    text = "Select order",
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 150.dp),
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold,
                    color = Color.White
                )
            } else {
                Text(
                    text = "My orders",
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 150.dp),
                    fontSize = 24.sp,
                    fontWeight = FontWeight.Bold,
                    color = Color.White
                )
            }
        }
    }
}

