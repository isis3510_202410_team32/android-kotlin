package com.example.foodloop.ui

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Button
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.foodloop.model.DTO.Meal
import com.example.foodloop.viewmodel.MealViewModel
import com.skydoves.landscapist.glide.GlideImage
import kotlinx.coroutines.launch

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun MealScreen() {
    val viewModel: MealViewModel = viewModel()
    var ingredient by remember { mutableStateOf("") }
    val mealsState by viewModel.meals.collectAsState()
    val scaffoldState = rememberScaffoldState()
    val coroutineScope = rememberCoroutineScope()
    val context = LocalContext.current
    var showError by remember { mutableStateOf(false) }

    MealScreenContent(
        ingredient = ingredient,
        onIngredientChange = { ingredient = it },
        onSearchClick = {
            if (isNetworkAvailable(context)) {
                viewModel.fetchMeals(ingredient)
                showError = false
            } else {
                coroutineScope.launch {
                    scaffoldState.snackbarHostState.showSnackbar(
                        "No internet connection. Please try again later."
                    )
                }
            }
        },
        showError = showError,
        mealsState = mealsState
    )
}

@Composable
fun MealScreenContent(
    ingredient: String,
    onIngredientChange: (String) -> Unit,
    onSearchClick: () -> Unit,
    showError: Boolean,
    mealsState: List<Meal>?
) {
    Column(modifier = Modifier
        .fillMaxSize()
        .padding(16.dp)) {
        Spacer(modifier = Modifier.height(48.dp))
        TextField(
            value = ingredient,
            onValueChange = onIngredientChange,
            label = { Text("Enter ingredient") },
            modifier = Modifier.fillMaxWidth()
        )
        Spacer(modifier = Modifier.height(8.dp))
        Button(
            onClick = onSearchClick,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text("Search")
        }
        Spacer(modifier = Modifier.height(16.dp))
        if (showError) {
            Text("No meals found with ingredient: $ingredient", color = Color.Red)
        }
        mealsState?.let { meals ->
            if (meals.isNotEmpty()) {
                LazyColumn {
                    items(meals) { meal ->
                        MealItem(meal)
                    }
                }
            } else {
                Text("Search meals by ingredient, try with chicken, salmon, beef or pork", color = Color.Gray)
            }
        }
    }
}

fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = connectivityManager.activeNetwork ?: return false
    val networkCapabilities = connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
    return networkCapabilities.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
}

@Composable
fun MealItem(meal: Meal) {
    Card(modifier = Modifier.padding(vertical = 8.dp)) {
        Row(modifier = Modifier.padding(16.dp)) {
            GlideImage(
                imageModel = meal.strMealThumb,
                contentScale = ContentScale.Crop,
                modifier = Modifier.size(64.dp),
                requestOptions = {
                    RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                }
            )
            Spacer(modifier = Modifier.width(16.dp))
            Text(meal.strMeal)
        }
    }
}

