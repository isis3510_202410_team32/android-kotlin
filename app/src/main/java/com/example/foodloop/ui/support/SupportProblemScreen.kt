package com.example.foodloop.ui.support


import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp

@Composable
fun SupportProblemScreen(

    modifier: Modifier = Modifier,
    onProblemButtonClicked: () -> Unit,
    onOtherProblemButtonClicked: () -> Unit,
    contentPadding: PaddingValues = PaddingValues(0.dp, 200.dp, 0.dp, 0.dp)
){
    Column (
        modifier.fillMaxSize()
    ){
        UpperBar()
        Spacer(Modifier.height(6.dp))
        Problem(onClick = onProblemButtonClicked)
        Spacer(Modifier.height(6.dp))
        OtherProblem(onClick = onOtherProblemButtonClicked)
    }
}

@Composable
fun Problem(modifier: Modifier = Modifier, onClick: () -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .clickable(onClick = onClick)
            .background(Color(0xFFF5F5F5))
    ) {
        Text(
            text = "Product in poor contition",
            color = Color.Black,
            style = TextStyle(
                fontSize = 14.sp
            ),
            modifier = Modifier.padding(30.dp)
        )
        Spacer(modifier = Modifier.width(110.dp))
        Icon(
            imageVector = Icons.Default.KeyboardArrowRight,
            contentDescription = null,
            modifier = Modifier.padding(end = 8.dp)
        )
    }
}

@Composable
fun OtherProblem( modifier: Modifier = Modifier, onClick: () -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .clickable(onClick = onClick)
            .background(Color(0xFFF5F5F5))
    ) {
        Text(
            text = "Products are missing     ",
            color = Color.Black,
            style = TextStyle(
                fontSize = 14.sp
            ),
            modifier = Modifier.padding(30.dp)
        )
        Spacer(modifier = Modifier.width(110.dp))
        Icon(
            imageVector = Icons.Default.KeyboardArrowRight,
            contentDescription = null,
            modifier = Modifier.padding(end = 8.dp)
        )
    }
}
