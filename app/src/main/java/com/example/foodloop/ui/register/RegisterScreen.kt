package com.example.foodloop.ui

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.OutlinedTextFieldDefaults
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.foodloop.FoodloopScreen
import com.example.foodloop.domain.UserDao
import com.example.foodloop.domain.service.EmailAuthStrategy
import com.example.foodloop.ui.theme.Green
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RegisterScreen(navController: NavHostController) {
    val (email, setEmail) = remember { mutableStateOf("") }
    val (fullName, setFullName) = remember { mutableStateOf("") }
    val (password, setPassword) = remember { mutableStateOf("") }
    val (confirmPassword, setConfirmPassword) = remember { mutableStateOf("") }
    val (emailError, setEmailError) = remember { mutableStateOf("") }
    val (fullNameError, setFullNameError) = remember { mutableStateOf("") }
    val (passwordError, setPasswordError) = remember { mutableStateOf("") }
    val (confirmPasswordError, setConfirmPasswordError) = remember { mutableStateOf("") }

    val context = LocalContext.current
    val emailAuthStrategy = remember { EmailAuthStrategy(context) }
    val userDao = UserDao(context)
    val sharedPreferences = context.getSharedPreferences("AppPreferences", Context.MODE_PRIVATE)

    var isConnected by remember { mutableStateOf(sharedPreferences.getBoolean("LastInternetStatus", false)) }
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    LaunchedEffect(key1 = Unit) {
        val connectionStatus = withContext(Dispatchers.IO) {
            checkInternetConnection(connectivityManager, sharedPreferences)
        }
        isConnected = connectionStatus
    }

    fun validateAndRegister() {
        var isValid = true
        setEmailError("")
        setFullNameError("")
        setPasswordError("")
        setConfirmPasswordError("")

        if (email.isBlank()) {
            setEmailError("Email cannot be empty")
            isValid = false
        }
        if (fullName.isBlank()) {
            setFullNameError("Full name cannot be empty")
            isValid = false
        }
        if (password.isBlank()) {
            setPasswordError("Password cannot be empty")
            isValid = false
        }
        if (confirmPassword.isBlank()) {
            setConfirmPasswordError("Confirm Password cannot be empty")
            isValid = false
        }

        if (isValid) {
            if (!isConnected) {
                Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show()
            } else {
                emailAuthStrategy.register(email, password, confirmPassword) { success, userId ->
                    if (success && userId != null) {
                        val newUser = com.example.foodloop.domain.User(
                            id = userId, email = email, name = fullName, password = password
                        )
                        userDao.createUser(newUser) { success ->
                            if (success) {
                                navController.navigate(FoodloopScreen.HomePage.name)
                            } else {
                                Toast.makeText(context, "No internet ", Toast.LENGTH_SHORT).show()
                            }
                        }
                    } else {
                        Toast.makeText(context, "Registration failed. Please try again.", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }

    Surface(
        modifier = Modifier.fillMaxSize(),
        color = Color.White
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.Start,
            verticalArrangement = Arrangement.Center
        ) {
            Spacer(modifier = Modifier.height(15.dp))
            Text(
                "Get Started!",
                style = MaterialTheme.typography.headlineMedium,
                textAlign = TextAlign.Left,
                color = Color.Black,
                fontWeight = FontWeight.Bold
            )
            Spacer(modifier = Modifier.height(48.dp))
            Text("Email", color= Color.Gray)
            OutlinedTextField(
                value = email,
                onValueChange = setEmail,
                label = { Text("Email") },
                modifier = Modifier.fillMaxWidth(),
                colors = OutlinedTextFieldDefaults.colors(
                    focusedContainerColor = MaterialTheme.colorScheme.surface,
                    unfocusedContainerColor = MaterialTheme.colorScheme.surface,
                    disabledContainerColor = MaterialTheme.colorScheme.surface,
                    focusedBorderColor = MaterialTheme.colorScheme.primary,
                    unfocusedBorderColor = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.12f),
                )
            )
            if (emailError.isNotEmpty()) Text(emailError, color = Color.Red)

            Spacer(modifier = Modifier.height(16.dp))
            Text("Full name", color= Color.Gray)
            OutlinedTextField(
                value = fullName,
                onValueChange = setFullName,
                label = { Text("Full name") },
                modifier = Modifier.fillMaxWidth(),
                colors = OutlinedTextFieldDefaults.colors(
                    focusedContainerColor = MaterialTheme.colorScheme.surface,
                    unfocusedContainerColor = MaterialTheme.colorScheme.surface,
                    disabledContainerColor = MaterialTheme.colorScheme.surface,
                    focusedBorderColor = MaterialTheme.colorScheme.primary,
                    unfocusedBorderColor = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.12f),
                )
            )
            if (fullNameError.isNotEmpty()) Text(fullNameError, color = Color.Red)

            Spacer(modifier = Modifier.height(16.dp))
            Text("Password", color= Color.Gray)
            OutlinedTextField(
                value = password,
                onValueChange = setPassword,
                label = { Text("Password") },
                visualTransformation = PasswordVisualTransformation(),
                modifier = Modifier.fillMaxWidth(),
                colors = OutlinedTextFieldDefaults.colors(
                    focusedContainerColor = MaterialTheme.colorScheme.surface,
                    unfocusedContainerColor = MaterialTheme.colorScheme.surface,
                    disabledContainerColor = MaterialTheme.colorScheme.surface,
                    focusedBorderColor = MaterialTheme.colorScheme.primary,
                    unfocusedBorderColor = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.12f),
                )
            )
            if (passwordError.isNotEmpty()) Text(passwordError, color = Color.Red)

            Spacer(modifier = Modifier.height(16.dp))
            Text("Confirm Password", color= Color.Gray)
            OutlinedTextField(
                value = confirmPassword,
                onValueChange = setConfirmPassword,
                label = { Text("Retype Password") },
                visualTransformation = PasswordVisualTransformation(),
                modifier = Modifier.fillMaxWidth(),
                colors = OutlinedTextFieldDefaults.colors(
                    focusedContainerColor = MaterialTheme.colorScheme.surface,
                    disabledContainerColor = MaterialTheme.colorScheme.surface,
                    unfocusedBorderColor = MaterialTheme.colorScheme.onSurface.copy(alpha = 0.12f),
                )
            )
            if (confirmPasswordError.isNotEmpty()) Text(confirmPasswordError, color = Color.Red)

            Spacer(modifier = Modifier.height(32.dp))
            Button(
                onClick = { validateAndRegister() },
                colors = ButtonDefaults.buttonColors(containerColor = Green),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
                    .border(1.dp, Green, shape = RectangleShape),
                shape= RoundedCornerShape(8.dp),
            ) {
                Text("Register", color = Color.White)
            }

            Spacer(modifier = Modifier.height(10.dp))
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                TextButton(onClick = { navController.navigate(FoodloopScreen.Authentication.name) }) {
                    Text("Have already an account? Log in", color = Green, fontWeight = FontWeight.Bold)
                }
            }
        }
    }
}



