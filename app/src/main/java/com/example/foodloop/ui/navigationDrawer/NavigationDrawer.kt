package com.example.foodloop.ui.navigationDrawer

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.DialogProperties
import androidx.navigation.NavHostController
import com.example.foodloop.R

@Composable
fun NavigationDrawer(
    modifier: Modifier = Modifier,
    onLogoutClick: () -> Unit,
    onSupportClick: () -> Unit,
    onSavingsClick: () -> Unit,
    onFavoriteListClick: () -> Unit,
    onSupermarketClick: () -> Unit,
    onMealsClick: () -> Unit,
    onRecipesClick: () -> Unit, // Añadir esta línea
    navController: NavHostController,
    context: Context
) {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val showNoInternetDialog = remember { mutableStateOf(false) }

    if (showNoInternetDialog.value) {
        AlertDialog(
            onDismissRequest = {
                showNoInternetDialog.value = false
            },
            title = { Text(text = "No Internet Connection", style = TextStyle(color = Color.White)) },
            text = {
                Text(
                    text = "Please check your internet connection and try again.",
                    style = TextStyle(color = Color.White)
                )
            },
            confirmButton = {
                TextButton(
                    onClick = {
                        showNoInternetDialog.value = false
                    }
                ) {
                    Text("OK", style = TextStyle(color = Color.White))
                }
            },
            properties = DialogProperties(
                dismissOnBackPress = true,
                dismissOnClickOutside = true,
                usePlatformDefaultWidth = false
            )
        )
    }

    Column {
        Spacer(Modifier.height(45.dp))
        DrawerHeader()
        DrawerBody(
            items = listOf(
                MenuItem("3", "My Meals", "My Meals", ImageVector.vectorResource(R.drawable.cookie3)),
                MenuItem("4", "My Savings", "My Savings", ImageVector.vectorResource(R.drawable.savings)),
                MenuItem("7", "Support", "Support", ImageVector.vectorResource(R.drawable.support)),
                MenuItem("8", "Supermarkets", "Supermarkets", Icons.Default.LocationOn),
                MenuItem("9", "Wasting", "Wasting", Icons.Default.Info),
            ),
            onItemClick = { /* No se hace nada por ahora */ },
            onSupportClick = {
                if (isInternetConnected(connectivityManager)) {
                    onSupportClick()
                } else {
                    navController.navigate("NoConnection")
                }
            },
            onSavingsClick = {
                if (isInternetConnected(connectivityManager)) {
                    onSavingsClick()
                } else {
                    navController.navigate("NoConnection")
                }
            },
            onFavoriteListClick = onFavoriteListClick,
            onSupermarketClick = {
                if (isInternetConnected(connectivityManager)) {
                    onSupermarketClick()
                } else {
                    showNoInternetDialog.value = true
                }
            },
            onMealsClick = {
                if (isInternetConnected(connectivityManager)) {
                    onMealsClick()
                } else {
                    showNoInternetDialog.value = true
                }
            },
            onRecipesClick = {
                if (isInternetConnected(connectivityManager)) {
                    onRecipesClick()
                } else {
                    showNoInternetDialog.value = true
                }
            }
        )

        DrawerBottom(onLogoutClick = onLogoutClick)
    }
}

@Composable
fun DrawerHeader() {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        // Imagen de perfil
        ProfileImage(
            modifier = Modifier
                .padding(top = 16.dp)
                .size(120.dp),
            image = painterResource(id = R.drawable.profile),
            contentDescription = "Profile Image"
        )

        Spacer(modifier = Modifier.height(16.dp))

        // Nombre
        Text(
            text = "User Name",
            fontSize = 24.sp
        )

        Spacer(modifier = Modifier.height(8.dp))

        // Correo
        Text(
            text = "correo@example.com",
            fontSize = 16.sp
        )
    }
}

@Composable
fun ProfileImage(
    modifier: Modifier = Modifier,
    image: Painter,
    contentDescription: String
) {
    Image(
        painter = image,
        contentDescription = contentDescription,
        modifier = modifier
            .clip(CircleShape),
        contentScale = ContentScale.Crop
    )
}

data class MenuItem(
    val id: String,
    val title: String,
    val contentDescription: String,
    val icon: ImageVector
)

@Composable
fun DrawerBody(
    items: List<MenuItem>,
    modifier: Modifier = Modifier,
    itemTextStyle: TextStyle = TextStyle(fontSize = 18.sp),
    onItemClick: (MenuItem) -> Unit,
    onSupportClick: () -> Unit,
    onSavingsClick: () -> Unit,
    onFavoriteListClick: () -> Unit,
    onMealsClick: () -> Unit,
    onSupermarketClick: () -> Unit,
    onRecipesClick: () -> Unit // Añadir esta línea
) {
    LazyColumn(modifier) {
        items(items) { item ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .clickable {
                        when (item.title) {
                            "Support" -> onSupportClick()
                            "My Savings" -> onSavingsClick()
                            "My Favorite List" -> onFavoriteListClick()
                            "Supermarkets" -> onSupermarketClick()
                            "My Meals" -> onMealsClick()
                            "Wasting" -> onRecipesClick() // Añadir esta lógica
                            else -> onItemClick(item)
                        }
                    }
                    .padding(16.dp)
            ) {
                Icon(
                    imageVector = item.icon,
                    contentDescription = item.contentDescription
                )
                Spacer(modifier = Modifier.width(16.dp))
                Text(
                    text = item.title,
                    style = itemTextStyle,
                    modifier = Modifier.weight(1f)
                )
            }
        }
    }
}

@Composable
fun DrawerBottom(onLogoutClick: () -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 16.dp),
        horizontalArrangement = Arrangement.Center
    ) {
        OutlinedButton(onClick = onLogoutClick) {
            Icon(contentDescription = "log Out", painter = painterResource(id = R.drawable.logout))
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Log Out")
        }
    }
}

private fun isInternetConnected(connectivityManager: ConnectivityManager): Boolean {
    val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    return capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
}
