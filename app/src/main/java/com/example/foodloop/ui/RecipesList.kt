package com.example.foodloop.ui

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyHorizontalGrid
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.foodloop.R
import com.example.foodloop.ui.theme.Green

@Preview(showBackground = true)
@Composable
fun RecipeList() {
    val context = LocalContext.current
    val imagePainter = painterResource(id = R.drawable.carulla)
    var mainImagen= painterResource(id = R.drawable.pasta)
    
    val recipes = listOf(
        Recipe1("Spaghetti Carbonara", "Medium", 600, "30m", imagePainter),
        Recipe1("Chicken Alfredo", "High", 800, "45m", imagePainter),
        Recipe1("Vegetable Stir-Fry", "Easy", 400, "20m", imagePainter),
        Recipe1("Grilled Salmon", "Medium", 700, "25m", imagePainter)
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(Green)
            .padding(bottom = 5.dp)
    ) {
        TopBar()
        Text(
            text = "Recipes List",
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(Alignment.CenterHorizontally)
                .padding(top = 16.dp),
            color = Color.White,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )
        mainImage(imagePainter = mainImagen)

        RecipeCardMain(title = "Spaghetti Carbonara", difficulty = "low", calories = 25, time ="30" , imagePainter =imagePainter )
        Text(
            text = "The most favorite recipes",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 5.dp),
            color = Color.White,
            fontWeight = FontWeight.Bold,
        )
        RecipeScrollableRow(recipes)
    }
}

@Composable
fun RecipeScrollableRow(recipes: List<Recipe1>) {
    LazyRow(
        modifier = Modifier
            .padding(8.dp)
            .background(Color.White)
    ) {
        items(recipes) { recipe ->
            RecipeCard(
                title = recipe.title,
                difficulty = recipe.difficulty,
                calories = recipe.calories,
                time = recipe.time,
                imagePainter = recipe.imagePainter
            )
        }
    }
}

@Composable
fun RecipeCardMain(
    title: String,
    difficulty: String,
    calories: Int,
    time: String,
    imagePainter: androidx.compose.ui.graphics.painter.Painter
) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(8.dp)
            .clickable { println("It has been clicked") },
        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp)
    ) {
        Box {
            Image(
                painter = imagePainter,
                contentDescription = title,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(180.dp)
            )
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(8.dp)
            ) {
                Text(
                    text = title,
                    fontWeight = FontWeight.Bold,
                    color = Color.White,
                    modifier = Modifier
                        .background(
                            Green
                        )
                        .padding(8.dp)
                )
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 1.dp)
                ) {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Color.White.copy(alpha = 0.5f))
                            .padding(end = 32.dp)
                    ) {
                        Column(modifier = Modifier.padding(8.dp)) {
                            Text(
                                text = "Difficulty: $difficulty",
                                color = Color.Black,
                                fontWeight = FontWeight.Bold
                            )
                            Text(
                                text = "$calories Cal",
                                color = Color.Black,
                                fontWeight = FontWeight.Bold
                            )
                        }
                        Column(modifier = Modifier.padding(8.dp)) {
                            Text(
                                text = "$time min",
                                color = Color.Black,
                                fontWeight = FontWeight.Bold
                            )
                        }
                    }
                }

            }

            }
        }
    }







@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar1() {
    TopAppBar(
        title = { Text(text = "Recipe List") },
        navigationIcon = {
            IconButton(onClick = { /*TODO*/ }) {
                Icon(Icons.Filled.ArrowBack, contentDescription = "Back")
            }
        },
        actions = {
            IconButton(onClick = { /*TODO*/ }) {
                Icon(Icons.Filled.ShoppingCart, contentDescription = "Cart")
            }
        },
        colors = TopAppBarDefaults.topAppBarColors()
    )
}
@Composable
fun RecipeCard(
    title: String,
    difficulty: String,
    calories: Int,
    time: String,
    imagePainter: androidx.compose.ui.graphics.painter.Painter
) {
    Column(modifier = Modifier.padding(8.dp)) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .clickable { println("Recipe Card clicked") },
            elevation = CardDefaults.cardElevation(defaultElevation = 4.dp)
        ) {
            Box {
                Image(
                    painter = imagePainter,
                    contentDescription = title,
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(150.dp)
                )
                Text(
                    text = title,
                    color = Color.White,
                    fontWeight = FontWeight.Bold,
                    modifier = Modifier
                        .align(Alignment.BottomStart)
                        .background(Color.Black.copy(alpha = 0.7f))
                        .fillMaxWidth()
                        .padding(4.dp)
                )
            }
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(start = 8.dp, end = 8.dp, top = 8.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_calories),
                contentDescription = "Calories",
                tint = Color.Black,
                modifier = Modifier.size(18.dp)
            )
            Spacer(modifier = Modifier.width(4.dp))
            Text(
                text = "$calories Cal",
                color = Color.Black
            )
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(start = 8.dp, end = 8.dp, bottom = 8.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_clock),
                contentDescription = "Time",
                tint = Color.Black,
                modifier = Modifier.size(18.dp)
            )
            Spacer(modifier = Modifier.width(4.dp))
            Text(
                text = time,
                color = Color.Black
            )
        }
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.padding(start = 8.dp, end = 8.dp, bottom = 8.dp)
        ) {

            Spacer(modifier = Modifier.width(3.dp))
            Text(
                text = "Difficulty: $difficulty",
                color = Color.Black,
                fontWeight = FontWeight.Bold


            )
        }
    }
}
@Composable
fun mainImage(    imagePainter: androidx.compose.ui.graphics.painter.Painter
)
{Image(
    painter = imagePainter,
    contentDescription = "main",
    modifier = Modifier
        .fillMaxWidth()
        .height(160.dp)
        .background(Color.White)
)
}