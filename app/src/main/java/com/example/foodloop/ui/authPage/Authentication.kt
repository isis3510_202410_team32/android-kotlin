package com.example.foodloop.ui

import android.content.Context
import android.net.ConnectivityManager
import android.widget.Toast
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.foodloop.FoodloopScreen
import com.example.foodloop.domain.service.EmailAuthStrategy
import com.example.foodloop.ui.theme.Green
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.regex.Pattern

@Composable
fun AuthenticationScreen(navController: NavHostController) {
    val context = LocalContext.current
    val sharedPreferences = context.getSharedPreferences("user_prefs", Context.MODE_PRIVATE)
    val (email, setEmail) = remember { mutableStateOf("") }
    val (password, setPassword) = remember { mutableStateOf("") }
    val (showError, setShowError) = remember { mutableStateOf(false) }
    val emailAuthStrategy = remember { EmailAuthStrategy(context) }

    var isConnected by remember { mutableStateOf(sharedPreferences.getBoolean("LastInternetStatus", false)) }
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    LaunchedEffect(key1 = Unit) {
        val connectionStatus = withContext(Dispatchers.IO) {
            checkInternetConnection(connectivityManager, sharedPreferences)
        }
        isConnected = connectionStatus
    }

    Surface(
        modifier = Modifier.fillMaxSize(),
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                "Login",
                style = MaterialTheme.typography.headlineMedium.copy(fontWeight = FontWeight.Bold),
            )
            Spacer(modifier = Modifier.height(48.dp))
            Text("Email", color = MaterialTheme.colorScheme.onSurfaceVariant)
            OutlinedTextField(
                value = email,
                onValueChange = {
                    setEmail(it)
                    if (showError && it.isBlank()) {
                        setShowError(false)
                    }
                },
                label = { Text("Email") },
                modifier = Modifier.fillMaxWidth()
            )
            if (!isEmailValid(email) && showError) {
                Text("Invalid email address", color = MaterialTheme.colorScheme.error)
            }

            Spacer(modifier = Modifier.height(16.dp))
            Text("Password", color = MaterialTheme.colorScheme.onSurfaceVariant)
            OutlinedTextField(
                value = password,
                onValueChange = {
                    setPassword(it)
                    if (showError && it.isBlank()) {
                        setShowError(true)
                    }
                },
                label = { Text("Password") },
                visualTransformation = PasswordVisualTransformation(),
                modifier = Modifier.fillMaxWidth()
            )
            if (password.isBlank() && showError) {
                Text("Password cannot be empty", color = MaterialTheme.colorScheme.error)
            }

            Spacer(modifier = Modifier.height(32.dp))
            Button(
                onClick = {
                    if (!isConnected) {
                        Toast.makeText(context, "No internet connection", Toast.LENGTH_SHORT).show()
                    } else if (email.isBlank() || password.isBlank()) {
                        setShowError(true)
                        Toast.makeText(context, "Please fill all fields", Toast.LENGTH_SHORT).show()
                    } else if (!isEmailValid(email)) {
                        setShowError(true)
                        Toast.makeText(context, "Please enter a valid email address", Toast.LENGTH_SHORT).show()
                    } else {
                        emailAuthStrategy.login(email, password) { success, userId ->
                            if (success && userId != null) {
                                sharedPreferences.edit().apply {
                                    putString("email", email)
                                    putString("password", password)
                                    putString("user_id", userId)
                                    apply()
                                }
                                navController.navigate(FoodloopScreen.HomePage.name)
                            } else {
                                Toast.makeText(context, "Login Failed. Please try again.", Toast.LENGTH_SHORT).show()
                            }
                        }
                    }
                },
                colors = ButtonDefaults.buttonColors(containerColor = Green),
                shape = RoundedCornerShape(8.dp),
                modifier = Modifier.fillMaxWidth().height(48.dp)
            ) {
                Text("Log in", color = MaterialTheme.colorScheme.onPrimary)
            }

            Spacer(modifier = Modifier.height(16.dp))
            Row(horizontalArrangement = Arrangement.Center, modifier = Modifier.fillMaxWidth()) {
                TextButton(onClick = { navController.navigate(FoodloopScreen.Register.name) }) {
                    Text(
                        "Don't have an account? Register",
                        color = MaterialTheme.colorScheme.primary,
                        fontWeight = FontWeight.Bold
                    )
                }
            }
        }
    }
}

fun isEmailValid(email: String): Boolean {
    val emailPattern = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$"
    return Pattern.compile(emailPattern).matcher(email).matches()
}

