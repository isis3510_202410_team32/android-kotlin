package com.example.foodloop.ui

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Snackbar
import androidx.compose.material3.SnackbarHost
import androidx.compose.material3.SnackbarHostState
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.example.foodloop.FoodloopScreen
import com.example.foodloop.ui.theme.Green
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Composable
fun LoginScreen(navController: NavHostController, context: Context) {
    val snackbarHostState = remember { SnackbarHostState() }
    val sharedPreferences = context.getSharedPreferences("AppPreferences", Context.MODE_PRIVATE)
    var isConnected by remember { mutableStateOf(sharedPreferences.getBoolean("LastInternetStatus", false)) }
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    LaunchedEffect(Unit) {
        isConnected = withContext(Dispatchers.IO) {
            checkInternetConnection(connectivityManager, sharedPreferences)
        }
    }

    var showSnackbar by remember { mutableStateOf(false) }

    LaunchedEffect(showSnackbar) {
        if (showSnackbar) {
            snackbarHostState.showSnackbar("No internet connection")
            showSnackbar = false
        }
    }

    Box(
        modifier = Modifier
            .fillMaxSize()

    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            GlideImage(url = "https://imgur.com/IsBqpJg.png", contentDescription = "Logo")
            Spacer(modifier = Modifier.height(32.dp))
            Text(
                "Cut Waste, Save Cash, Join FoodLoop!",
                style = MaterialTheme.typography.headlineMedium,
                textAlign = TextAlign.Center,
                color = Green
            )
            Spacer(modifier = Modifier.height(32.dp))

            Button(
                onClick = {
                    if (!isConnected) {
                        showSnackbar = true
                    } else {
                        navController.navigate(FoodloopScreen.Register.name)
                    }
                },
                colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
                    .border(2.dp, Green),
                contentPadding = PaddingValues()
            ) {
                Text("Get started", color = Green)
            }

            Spacer(modifier = Modifier.height(16.dp))

            Button(
                onClick = {
                    if (!isConnected) {
                        showSnackbar = true
                    } else {
                        navController.navigate(FoodloopScreen.Authentication.name)
                    }
                },
                colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(48.dp)
                     .border(2.dp, Green),

            contentPadding = PaddingValues()
            ) {
                Text("Log in", color = Green)
            }

            Spacer(modifier = Modifier.height(24.dp))
            Text(
                "By joining FoodLoop, you agree to our Terms of service and Privacy policy",
                style = MaterialTheme.typography.bodySmall,
                textAlign = TextAlign.Center,
                color = Color.Gray
            )
        }

        SnackbarHost(
            hostState = snackbarHostState,
            modifier = Modifier.align(Alignment.BottomCenter)
        ) { data ->
            Snackbar(
                snackbarData = data,
                containerColor = Color.White,
                contentColor = Color.Black,
                actionColor = Color.Black
            )
        }
    }
}

@Composable
fun GlideImage(url: String, contentDescription: String? = null) {
    val context = LocalContext.current
    var imageBitmap by remember { mutableStateOf<Bitmap?>(null) }

    DisposableEffect(url) {
        val glideRequest = Glide.with(context)
            .asBitmap()
            .load(url)
            .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))

        val target = object : CustomTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                imageBitmap = resource
            }

            override fun onLoadCleared(placeholder: Drawable?) {}
        }

        glideRequest.into(target)

        onDispose {
            Glide.with(context).clear(target)
        }
    }

    imageBitmap?.let {
        Image(
            bitmap = it.asImageBitmap(),
            contentDescription = contentDescription,
            modifier = Modifier
                .fillMaxWidth()
                .height(350.dp)
        )
    }
}

suspend fun checkInternetConnection(
    connectivityManager: ConnectivityManager,
    sharedPreferences: SharedPreferences
): Boolean {
    return try {
        withContext(Dispatchers.IO) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            val isConnected =
                capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
            sharedPreferences.edit().apply {
                putBoolean("LastInternetStatus", isConnected)
                apply()
            }
            isConnected
        }
    } catch (e: Exception) {
        sharedPreferences.getBoolean("LastInternetStatus", false)
    }
}
