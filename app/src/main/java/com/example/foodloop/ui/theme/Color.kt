package com.example.foodloop.ui.theme

import androidx.compose.ui.graphics.Color

val Green = Color(0xFF0A5550)
val GreenTwo = Color(0xFF255450)
val DarkGreen = Color(0xFF042A2B)
val GreenThree = Color(0xFF0A5550)
val LightGreen = Color(0xFF9AC2AE)
val FoodLoopGreen = Color(0xFFCEEFC8)
val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)
val GreenBackground = Color(0xFFCEEFC8)
val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)
val GrayL= Color(0xFF8A8888)