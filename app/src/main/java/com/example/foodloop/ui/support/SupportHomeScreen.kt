package com.example.foodloop.ui.support

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.foodloop.R


@Composable
fun SupportHomeScreen(

    modifier: Modifier = Modifier,
    onOrderButtonClicked: () -> Unit,
    contentPadding: PaddingValues = PaddingValues(0.dp, 200.dp, 0.dp, 0.dp)
){
    Column (
        modifier.fillMaxSize()
    ){
        UpperBar()
        Orders(Modifier.fillMaxSize(), onClick = onOrderButtonClicked)
    }
}

@Composable
fun Orders(
    modifier: Modifier = Modifier,
    dates: List<String> = List(10) { "Fecha orden ${it+1}" },
    onClick: () -> Unit
) {
    LazyColumn(
        contentPadding = PaddingValues(6.dp, 6.dp, 6.dp, 6.dp),
        modifier = modifier
            .padding(4.dp)
            .fillMaxSize()
    ) {
        items(items = dates) { date ->
            Order(orderDate = date, onClick = onClick)
            Spacer(Modifier.height(6.dp))
        }
    }
}

@Composable
fun Order(orderDate: String, modifier: Modifier = Modifier, onClick: () -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .clickable(
                onClick = onClick
            )
            .background(Color(0xFFF5F5F5))
    ) {
        Image(
            painter = painterResource(id = R.drawable.carulla),
            contentDescription = "image 60",
            modifier = Modifier
                .padding(8.dp)
                .clip(shape = RoundedCornerShape(8.dp))
        )

        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = orderDate,
                color = Color.Black,
                style = TextStyle(
                    fontSize = 14.sp
                ),
                modifier = Modifier
                    .padding(8.dp)
            )
            Icon(
                imageVector = Icons.Default.KeyboardArrowRight,
                contentDescription = null,
                modifier = Modifier.padding(end = 8.dp)
            )
        }
    }
}

