package com.example.foodloop.ui.search

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.requiredWidth
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Clear
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.shadow
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.foodloop.MainViewModel
import com.example.foodloop.R
import com.example.foodloop.ui.theme.Green
import com.example.foodloop.ui.theme.GreenTwo

@Composable
fun SearchScreen(viewModel: MainViewModel, modifier: Modifier = Modifier) {
    val products = viewModel.products.collectAsState()

    //Log.d("SearchScreen", "Products: ${products.value}")

    Column(
        modifier
            .verticalScroll(rememberScrollState())
    ) {
        SearchBar(viewModel, Modifier.padding(horizontal = 16.dp))
        Spacer(modifier = Modifier.height(30.dp))
        ProductByStore()
        ProductByStore()
        ProductByStore()
    }
}

@Composable
fun ProductByStore() {
    Column {
        Store()
        ProductList()
        Spacer(Modifier.height(25.dp))
    }
}

@Composable
fun ProductList() {
    LazyRow {
        items(4) {
            Product(modifier = Modifier)
        }
    }
}

@Composable
fun Store(modifier: Modifier = Modifier) {
    Row(modifier = Modifier
        .fillMaxWidth()
        .padding(horizontal = 20.dp)
        .background(Color(0XFFFFFFFF))
    ) {
        Box(
            modifier = Modifier.size(50.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.carulla),
                contentDescription = "Mi Imagen",
                modifier = Modifier.fillMaxSize()
            )
        }

        Column(
            modifier = Modifier.padding(start = 8.dp)
        ) {
            Text(
                text = "Store name",
                color = Color.Black,
                style = TextStyle(fontSize = 14.sp)
            )
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(top = 4.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.scooter),
                    contentDescription = "Icono de Moto",
                    modifier = Modifier.size(24.dp)
                )
                Text(
                    text = "30-35min",
                    color = Color.Black,
                    style = TextStyle(fontSize = 12.sp),
                    modifier = Modifier.padding(start = 8.dp)
                )
            }
        }
    }
}

@Composable
fun SearchBar(viewModel: MainViewModel, modifier: Modifier = Modifier){
    val searchText = viewModel.searchText.collectAsState().value
    val greenTwo = GreenTwo
    val roundedCornerShape = RoundedCornerShape(16.dp)

    Column {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .heightIn(min = 56.dp)
                .background(color = Green)
        ){
            TextField(
                value = searchText,
                onValueChange = viewModel::onSearchTextChange,
                leadingIcon = {
                    Icon(
                        imageVector = Icons.Default.Search,
                        contentDescription = null,
                        tint = Color.White
                    )
                },
                trailingIcon = {
                    Icon(
                        imageVector = Icons.Default.Clear,
                        contentDescription = null,
                        tint = Color.White
                    )
                },

                colors = TextFieldDefaults.colors(
                    unfocusedContainerColor = greenTwo,
                    focusedContainerColor = greenTwo,
                    cursorColor = Color.Black,
                    ),
                placeholder = {
                    Text("Search product", color = Color.White)
                },
                shape = roundedCornerShape,
                modifier = modifier
                    .fillMaxWidth()
                    .heightIn(min = 56.dp)
            )
        }
    }
}

@Composable
fun Product(modifier: Modifier = Modifier) {
    Column(
        modifier = modifier
            .padding(horizontal = 2.dp)
            .background(Color(0xFFFFFFFF))
            .requiredWidth(120.dp)
    ){
        Box(
            modifier = Modifier
                .padding(vertical = 8.dp)
                .shadow(4.dp)
                .clip(shape = RoundedCornerShape(10.dp) )
                .align(Alignment.CenterHorizontally)
        ){
            Image(
                painter = painterResource(id = R.drawable.margarita_natural),
                contentDescription = "image 75",
                modifier = Modifier
                    .requiredHeight(height = 90.dp)
            )
        }

        Text(
            text = "Papas Margarita",
            color = Color.Black,
            style = TextStyle(fontSize = 14.sp),
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
                .align(Alignment.CenterHorizontally)
        )
        Spacer(modifier = Modifier.height(8.dp))
        Row(
            horizontalArrangement = Arrangement.SpaceAround,
            modifier = Modifier.fillMaxWidth()
        ){
            Text(
                text = "$1200",
                color = Color.DarkGray,
                style = TextStyle(
                    fontSize = 12.sp,
                    textDecoration = TextDecoration.LineThrough
                )
            )
            Icon(
                imageVector = Icons.Default.ArrowForward,
                contentDescription = null,
                tint = Color.Gray
            )
            Text(
                text = "$900",
                color = Color.DarkGray,
                style = TextStyle(fontSize = 12.sp)
            )
        }
        TextButton(
            onClick = { },
            colors = ButtonDefaults.buttonColors(containerColor = Color.Transparent),
            modifier = Modifier
                .background(color = Green, shape = RoundedCornerShape(10.dp))
                .align(Alignment.CenterHorizontally)
        ) {
            Text(
                text = "Add to cart",
                color = Color.White,
                textAlign = TextAlign.Center,
                style = TextStyle(fontSize = 13.sp),
                modifier = Modifier.requiredHeight(height = 16.dp)
            )
        }
    }
}