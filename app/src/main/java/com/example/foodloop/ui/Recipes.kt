package com.example.foodloop.ui

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.foundation.lazy.grid.rememberLazyGridState
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.foodloop.R
import com.example.foodloop.ui.theme.Green
import com.example.foodloop.ui.theme.GreenBackground

data class Recipe(
    val name: String,
    val imageUrl: Int
)


class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val favoriteRecipes = listOf(
                Recipe("Lasagna", R.drawable.carulla),
                Recipe("Pasta", R.drawable.carulla),
                Recipe("Bandeja paisa", R.drawable.carulla),
                Recipe("Spanish omelette", R.drawable.carulla),
            )
            Box(modifier = Modifier.background(Green)) {
                Column {
                    TopBar()
                    RecipesList(recipes = favoriteRecipes)
                }
            }
        }
    }
}
@Composable
fun RecipesList(recipes: List<Recipe>) {
    val gridState = rememberLazyGridState()
    LazyVerticalGrid(
        columns = GridCells.Fixed(2),

        state = gridState,
        contentPadding = PaddingValues(8.dp),
        content = {
            items(recipes) { recipe ->
                RecipeCard(recipe = recipe)
            }
        }
    )
}

@Composable
fun RecipeCard(recipe: Recipe) {
    val context = LocalContext.current
    Card(
        modifier = Modifier
            .padding(8.dp)
            .fillMaxWidth()
            .aspectRatio(1.3f)
            .clickable {
                Toast
                    .makeText(context, "Clicked on ${recipe.name}", Toast.LENGTH_SHORT)
                    .show()
            },
    ) {
        Box {
            Image(
                painter = painterResource(id = recipe.imageUrl),
                contentDescription = recipe.name,
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.Crop
            )
            Box(
                modifier = Modifier
                    .matchParentSize()
                    .background(
                        Brush.verticalGradient(
                            colors = listOf(Color.Transparent, Color.Black),
                            startY = 350f
                        )
                    )
            )
            Text(
                text = recipe.name,
                color= Color.White,
                fontWeight = FontWeight.Bold,
                modifier = Modifier

                    .align(Alignment.BottomStart)
                    .padding(8.dp),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar() {
    TopAppBar(
        title = {
            Row(
                modifier = Modifier.fillMaxWidth(),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(text = "Food", color = Color.White, fontWeight = FontWeight.Bold)
                Text(text = "Loop", color = GreenBackground, fontWeight = FontWeight.Bold)
            }
        },
        navigationIcon = {
            IconButton(onClick = { /* Handle navigation icon click */ }) {
                Icon(imageVector = Icons.Default.ArrowBack, contentDescription = "Menu", tint = Color.White)
            }
        },
        actions = {

            Spacer(modifier = Modifier.weight(1f, fill = false))
            IconButton(onClick = { /* Handle shopping cart click */ }) {
                Icon(imageVector = Icons.Default.ShoppingCart, contentDescription = "Shopping Cart", tint = Color.White)
            }
        },
        colors = TopAppBarDefaults.topAppBarColors(containerColor = Green, titleContentColor = Color.White)
    )
}


@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    val favoriteRecipes = listOf(
        Recipe("Lasagna", R.drawable.carulla),
        Recipe("Pasta", R.drawable.carulla),
        Recipe("Bandeja paisa", R.drawable.carulla),
        Recipe("Spanish omelette", R.drawable.carulla),
    )

    Box(modifier = Modifier.background(Green)) {
        Column {
            TopBar() // Your TopBar composable
            Text(
                text = "Favorite Recipes",
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentWidth(Alignment.CenterHorizontally),
                color = Color.White,
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center
            )
            
            RecipesList(recipes = favoriteRecipes)
            Box(modifier = Modifier.background(Color.White)) {

            Column {
                HeaderComposable(text = "Search about recipes you can do!" , textColor= Color.Gray)

                val productRecommendedImages = listOf(R.drawable.margarita_natural, R.drawable.margarita_natural, R.drawable.margarita_natural, R.drawable.margarita_natural)
                val productRecommendedNames = listOf("Margarita", "Agua de Coco", "Agua de Coco", "Agua de Coco")
                ImageProductButtons(productImages = productRecommendedImages, productNames =productRecommendedNames )

            }}
        }

    }
}