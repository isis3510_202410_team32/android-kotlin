package com.example.foodloop.ui.supermarkets

import androidx.compose.runtime.Composable
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Text
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.runtime.remember
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.compose.ui.text.font.FontWeight
import androidx.core.content.ContextCompat
import com.example.foodloop.ui.theme.Green
import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import androidx.core.app.ActivityCompat
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.util.Log
import android.widget.Toast
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.TextButton
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.window.DialogProperties
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.foodloop.viewmodel.supermarkets.SupermarketViewModel
import com.google.android.gms.location.LocationServices
import com.example.foodloop.model.Store
import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition



@Composable
fun SuppermarketList(

    modifier: Modifier = Modifier,
    onSupermarketClick: (storeId: String) -> Unit,
    contentPadding: PaddingValues = PaddingValues(0.dp, 200.dp, 0.dp, 0.dp)
){
    val supermarketViewModel: SupermarketViewModel = viewModel()
    val context = LocalContext.current
    val fusedLocationClient = remember { LocationServices.getFusedLocationProviderClient(context) }
    val data by supermarketViewModel.supermarketsData.collectAsState()
    val gpsWarning = remember { mutableStateOf(false) }

    val isConnected by remember { mutableStateOf(checkInternetConnection(context)) }

    if (isConnected) {
        LaunchedEffect(key1 = true) {
            requestLocationPermission(context)
            if (ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    context,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                Log.d("GPS", "Error de permisos")
                return@LaunchedEffect
            }

            val mLocationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val mGPS = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)

            if(!mGPS){
                gpsWarning.value = true
                return@LaunchedEffect
            } else {
                gpsWarning.value = false
            }

            fusedLocationClient.lastLocation
                .addOnSuccessListener { location ->

                    // location.latitude y location.longitude

                    supermarketViewModel.fetchSupermarketsData(location.latitude, location.longitude)

                    Log.d("GPS", "SuppermarketList: ${location.latitude}, ${location.longitude} ")
                }
                .addOnFailureListener{

                    Log.d("GPS", it.message ?: "Error")
                }
        }

        Column (
            modifier.fillMaxSize()
        ){
            Title()

            if(gpsWarning.value){
                AlertDialog(
                    onDismissRequest = {
                        gpsWarning.value = false
                    },
                    title = { androidx.compose.material.Text(text = "No GPS Activated", style = TextStyle(color = Color.White))},
                    text = {
                        androidx.compose.material.Text(
                            text = "Please turn on location services.",
                            style = TextStyle(color = Color.White)
                        )
                    },
                    confirmButton = {
                        TextButton(
                            onClick = {
                                gpsWarning.value = false
                            }
                        ) {
                            androidx.compose.material.Text("OK", style = TextStyle(color = Color.White))

                        }
                    },
                    properties = DialogProperties(
                        dismissOnBackPress = true,
                        dismissOnClickOutside = true,
                        usePlatformDefaultWidth = false

                    )
                )
            }

            Supermarkets(Modifier.fillMaxSize(), data, onClick = onSupermarketClick)
        }
    } else {
        Toast.makeText(
            context,
            "You don't have an internet connection. Please check your connection and try again.",
            Toast.LENGTH_SHORT
        ).show()
    }


}

fun checkInternetConnection(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val network = connectivityManager.activeNetwork
    val networkCapabilities = connectivityManager.getNetworkCapabilities(network)
    return networkCapabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) == true
}

@Composable
fun Supermarkets(
    modifier: Modifier = Modifier,
    supermarkets: Array<Store>,
    onClick: (storeId: String) -> Unit
) {
    var visibleItems by remember { mutableStateOf(5) }
    LazyColumn(
        contentPadding = PaddingValues(6.dp, 6.dp, 6.dp, 6.dp),
        modifier = modifier
            .padding(4.dp)
            .fillMaxSize()
    ) {
        items(items = supermarkets.take(visibleItems)) {
            Supermarket(supermarket = it, onClick = onClick)
            Spacer(Modifier.height(6.dp))
        }
    }
}

@Composable
fun Supermarket(supermarket : Store, modifier: Modifier = Modifier, onClick: (storeId: String) -> Unit) {
    val context = LocalContext.current
    val isConnected = remember { mutableStateOf(false) }

    DisposableEffect(Unit) {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val callback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {
                isConnected.value = true
            }

            override fun onLost(network: Network) {
                isConnected.value = false
            }
        }

        val networkRequest = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()

        connectivityManager.registerNetworkCallback(networkRequest, callback)

        // Desregistrarse cuando se termina la composición
        onDispose {
            connectivityManager.unregisterNetworkCallback(callback)
        }
    }

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .clickable(onClick = {
                //if (isConnected.value) {
                    onClick(supermarket.id)
                /*} else {
                    Toast.makeText(
                        context,
                        "You don't have an internet connection. Please check your connection and try again.",
                        Toast.LENGTH_SHORT
                    ).show()
                }*/
            })
            .background(Color(0xFFF5F5F5))
    ) {
        ImageFromUrl(
            url = supermarket.imageUrl, contentDescription = "Supermarket image"
        )

        Column(
            modifier = Modifier.weight(1f)
        ) {
            Text(
                text = supermarket.name,
                color = Color.Black,
                style = TextStyle(
                    fontSize = 20.sp
                ),
                modifier = Modifier
                    .padding(8.dp)
            )

            Spacer(modifier = Modifier.height(4.dp))
            Text(
                text = "Distance: ${String.format("%.1f", supermarket.distance)} km",
                color = Color.Black,
                style = TextStyle(
                    fontSize = 14.sp
                ),
                modifier = Modifier
                    .padding(8.dp)
            )
        }
    }
}





@Composable
fun ImageFromUrl(url: String, contentDescription: String? = null, modifier: Modifier = Modifier) {
    val context = LocalContext.current

    // Glide setup
    val glideRequest = Glide.with(context)
        .asBitmap()
        .load(url)
        .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.ALL))

    val imageBitmap = remember { mutableStateOf<Bitmap?>(null) }

    GlideImage(
        request = glideRequest,
        onLoadingFinished = { bitmap ->
            imageBitmap.value = bitmap
        }
    )

    imageBitmap.value?.let {
        Image(
            bitmap = it.asImageBitmap(),
            contentDescription = contentDescription
        )
    }
}

@Composable
fun GlideImage(
    request: RequestBuilder<Bitmap>,
    onLoadingFinished: (Bitmap) -> Unit
) {
    DisposableEffect(key1 = request) {
        val target = object : CustomTarget<Bitmap>() {
            override fun onResourceReady(
                resource: Bitmap,
                transition: Transition<in Bitmap>?
            ) {
                onLoadingFinished(resource)
            }

            override fun onLoadCleared(placeholder: Drawable?) {
                // No-op
            }
        }

        request.into(target)

        onDispose {
            //Glide.with(target).clear(target)
        }
    }
}

@Composable
fun Title (modifier: Modifier = Modifier) {
    NavigationBar(
        containerColor = Green,
        modifier = modifier.requiredHeight(36.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {

            Text(
                text = "Supermarkets",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 120.dp),
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White
            )
        }
    }
}

private fun requestLocationPermission(context: Context) {
    // Verificar si el permiso de ubicación ya está otorgado
    if (ContextCompat.checkSelfPermission(
            context,
            Manifest.permission.ACCESS_FINE_LOCATION
        ) != PackageManager.PERMISSION_GRANTED
    ) {
        // Si no se otorgó, solicitar permiso de ubicación
        ActivityCompat.requestPermissions(
            context as Activity,
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            1001
        )
    }
}

private fun isInternetConnected(connectivityManager: ConnectivityManager): Boolean {
    val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    return capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
}
