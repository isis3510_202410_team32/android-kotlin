package com.example.foodloop.ui

import android.app.Application
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.example.foodloop.model.LocalFoodWaste
import com.example.foodloop.model.SupermarketWaste
import com.example.foodloop.ui.theme.Green
import com.example.foodloop.viewmodel.WasteViewModel
import com.example.foodloop.viewmodel.WasteViewModelFactory

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun WasteList() {
    val context = LocalContext.current
    val factory = WasteViewModelFactory(context.applicationContext as Application)
    val wasteViewModel: WasteViewModel = viewModel(factory = factory)

    val supermarketWastes by wasteViewModel.supermarketWastes.observeAsState()
    val localFoodWastes by wasteViewModel.localFoodWastes.observeAsState(emptyList())

    var showDialog by remember { mutableStateOf(false) }
    var name by remember { mutableStateOf("") }
    var foodType by remember { mutableStateOf("") }
    var weight by remember { mutableStateOf("") }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(Green)
            .padding(bottom = 5.dp)
    ) {
        TopBar1()
        Text(
            text = "Supermarket Waste List",
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentWidth(Alignment.CenterHorizontally)
                .padding(top = 16.dp),
            color = Color.White,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )

        LazyRow {
            items(supermarketWastes ?: emptyList()) { waste ->
                SupermarketWasteCard(waste)
            }
        }

        Text(
            text = "Your Wasting History",
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp),
            color = Color.White,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )
        Button(onClick = { showDialog = true }) {
            Text(text = "Add")
        }
        LazyColumn {
            items(localFoodWastes) { waste ->
                LocalFoodWasteCard(waste)
            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 5.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween
        ) {

        }
    }

    if (showDialog) {
        AlertDialog(
            onDismissRequest = { showDialog = false },
            title = { Text(text = "Add Wasting") },
            text = {
                Column {
                    OutlinedTextField(
                        value = name,
                        onValueChange = { name = it },
                        label = { Text("Name") }
                    )
                    OutlinedTextField(
                        value = foodType,
                        onValueChange = { foodType = it },
                        label = { Text("Food Type") }
                    )
                    OutlinedTextField(
                        value = weight,
                        onValueChange = { weight = it },
                        label = { Text("Weight (approximate)") },
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                    )
                }
            },
            confirmButton = {
                Button(
                    onClick = {
                        wasteViewModel.insertLocalFoodWaste(name, foodType, weight.toDoubleOrNull() ?: 0.0)
                        showDialog = false
                        name = ""
                        foodType = ""
                        weight = ""
                    }
                ) {
                    Text("Add")
                }
            },
            dismissButton = {
                Button(onClick = { showDialog = false }) {
                    Text("Cancel")
                }
            }
        )
    }
}

@Composable
fun SupermarketWasteCard(supermarketWaste: SupermarketWaste) {
    Card(
        modifier = Modifier
            .padding(8.dp)
            .clickable { println("Supermarket Waste Card clicked") },
        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp)
    ) {
        Column(
            modifier = Modifier
                .width(200.dp)
                .padding(8.dp)
        ) {
            Image(
                painter = rememberImagePainter(data = supermarketWaste.imageUrl),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxWidth()
                    .height(100.dp),
                contentScale = ContentScale.Crop
            )
            Text(
                text = supermarketWaste.name,
                fontWeight = FontWeight.Bold,
                color = Color.Black,
                modifier = Modifier.padding(top = 8.dp, bottom = 4.dp)
            )
            Text(
                text = "Address: ${supermarketWaste.address}",
                color = Color.Gray,
                modifier = Modifier.padding(bottom = 4.dp)
            )
            Text(
                text = "Waste: %.2f kg".format(supermarketWaste.waste),
                color = Color.Gray
            )
        }
    }
}

@Composable
fun LocalFoodWasteCard(localFoodWaste: LocalFoodWaste) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp)
            .clickable { println("Local Food Waste Card clicked") },
        elevation = CardDefaults.cardElevation(defaultElevation = 4.dp)
    ) {
        Column(modifier = Modifier.padding(8.dp)) {
            Text(
                text = localFoodWaste.name,
                fontWeight = FontWeight.Bold,
                color = Color.Black,
                modifier = Modifier.padding(bottom = 4.dp)
            )
            Text(
                text = "Food Type: ${localFoodWaste.foodType}",
                color = Color.Gray,
                modifier = Modifier.padding(bottom = 4.dp)
            )
            Text(
                text = "Weight: %.2f kg".format(localFoodWaste.weight),
                color = Color.Gray
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun TopBar1() {
    TopAppBar(
        title = { Text(text = "Food Waste Management") },
        navigationIcon = {
            // Opcional: Agregar un icono de navegación si es necesario
        },
        actions = {
            // Opcional: Agregar acciones adicionales si es necesario
        },
        colors = TopAppBarDefaults.topAppBarColors()
    )
}
