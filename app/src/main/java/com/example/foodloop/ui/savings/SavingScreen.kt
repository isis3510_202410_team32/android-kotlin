package com.example.foodloop.ui.savings
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.foodloop.ui.theme.DarkGreen
import com.example.foodloop.ui.theme.Green
import com.example.foodloop.ui.theme.GreenThree
import com.example.foodloop.viewmodel.savings.SavingsViewModel
import java.util.Calendar

@Composable
fun SavingScreen(modifier: Modifier = Modifier) {
    val savingsViewModel: SavingsViewModel = viewModel()
    val months = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dic")
    val dataFS by savingsViewModel.savingsData.collectAsState()

    // State para almacenar el resultado obtenido de la Cloud Function
    var result by remember { mutableStateOf("") }

    LaunchedEffect(key1 = true) {
        val calendar = Calendar.getInstance()
        savingsViewModel.fetchSavingsData("0cb485bb-8f41-431d-9f7a-d3e17217d4b9", calendar.get(Calendar.YEAR))
    }

    var startIndex by remember { mutableStateOf(0) }

    val visibleData = dataFS.slice(startIndex until (startIndex + 5).coerceAtMost(dataFS.size))

    var selectedIndex by remember { mutableStateOf(0) }
    var selectedMonth by remember { mutableStateOf("") }
    var selectedValue by remember { mutableStateOf(0f) }
    val accumulatedSavings = dataFS.sum()

    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier
            .fillMaxSize()

    ) {
        Column(
            modifier = Modifier
                .padding(16.dp)
                .verticalScroll(rememberScrollState())
        ) {
            Spacer(modifier = Modifier.height(20.dp))
            TextSection(
                selectedMonth = selectedMonth,
                selectedValue = selectedValue,
                accumulatedSavings = accumulatedSavings
            )
            Spacer(modifier = Modifier.height(10.dp))
            Box(
                modifier = Modifier
                    .fillMaxWidth()
            ) {
                BarChart(data = visibleData, startIndex = startIndex) { index ->
                    selectedIndex = index + startIndex
                    selectedMonth = months[index + startIndex]
                    selectedValue = dataFS[index + startIndex]
                }
            }
            Amount(
                dataFS.toList(),
                onBack = {
                    if (startIndex > 0) {
                        startIndex -= 1
                    }
                },
                onForward = {
                    if (startIndex + 5 < dataFS.size) {
                        startIndex += 1
                    }
                },
                startIndex = startIndex,
                selectedIndex = selectedIndex
            )
        }
    }
}


@Composable
fun BarChart(data: List<Float>, startIndex: Int, onIndexSelected: (Int) -> Unit) {
    val saturationStep = 0.25f // Incremento de saturación entre cada barra
    val baseHue = 150f // Matiz base
    val baseSaturation = 0.25f // Saturación base
    val maxBarHeight = 100.dp // Altura máxima de la barra en dp
    val months = arrayOf("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dic")

    // Normalizar los datos
    val maxValue = data.maxOrNull()?.toFloat() ?: 0F
    Log.d("Firestore", "máximo: $maxValue")
    val normalizedData = data.map { if (maxValue == 0F){ 0F } else {it / maxValue} }//.toFloatArray()  //.map {  it to it / maxValue }

    val scaleFactor = if (maxValue > 0) maxBarHeight.value else 0f

    Box {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.Bottom
        ) {
            Spacer(modifier = Modifier.width(20.dp))
            normalizedData.mapIndexed() { index, value ->
                val hue = baseHue
                val saturation = baseSaturation
                val scaledValue = value * scaleFactor
                Log.d("Firestore", "value: $value")
                val color = calculateColor(hue, saturation, 0.4f, value)

                Box(
                    modifier = Modifier
                        .weight(1f)
                        .clickable {
                            onIndexSelected(index)
                        }
                ) {
                    Column(
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Spacer(modifier = Modifier.height(4.dp))
                        Box(
                            modifier = Modifier
                                .height(scaledValue.dp)
                                .fillMaxWidth()
                                .background(color = color, shape = RoundedCornerShape(8.dp))
                        )
                        Text(text = months[index+startIndex], color = Green)

                    }
                }
                Spacer(modifier = Modifier.width(20.dp))
            }
        }
    }
}



fun calculateColor(hue: Float, saturation: Float, luminance: Float, alpha: Float): Color {
    val colorInt = android.graphics.Color.HSVToColor(floatArrayOf(hue, saturation, luminance))
    return Color(colorInt).copy(alpha = alpha)
}
@Composable
fun Amount(data: List<Float>, onForward: () -> Unit,
           onBack: () -> Unit ,startIndex: Int, selectedIndex: Int, modifier: Modifier = Modifier){
    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.Center
    ) {
        IconButton(
            onClick = { onBack() }
        ) {
            Icon(Icons.Default.ArrowBack, contentDescription = "Left arrow")
        }
        Text(text = data.getOrNull(selectedIndex)?.toString() ?: "", color = Green)
        IconButton(
            onClick = { onForward() }
        ) {
            Icon(Icons.Default.ArrowForward, contentDescription = "Right arrow")
        }
    }
}

@Composable
fun TextSection(selectedMonth: String,
                selectedValue: Float,
                accumulatedSavings: Float) {
    Text(
        text = "Your Savings",
        style = TextStyle(
            fontWeight = FontWeight.Bold,
            fontSize = 30.sp,
            color = DarkGreen
        ),
        modifier = Modifier.padding(bottom = 8.dp, top = 16.dp) // Ajuste en la parte superior
    )
    Text(
        text = "In $selectedMonth you saved",
        fontSize = 20.sp,
        color = GreenThree,
        modifier = Modifier.padding(bottom = 8.dp)
    )
    Text(
        text = "$$selectedValue",
        fontSize = 32.sp,
        color = Color.Black,
        modifier = Modifier.padding(bottom = 8.dp)
    )
    Text(
        text = "Accumulated savings: $accumulatedSavings",
        fontSize = 16.sp,
        color = Green,
        modifier = Modifier.padding(bottom = 8.dp)
    )
    Text(
        text = "Record",
        fontSize = 20.sp,
        color = Green,
        modifier = Modifier.padding(bottom = 8.dp)
    )
}