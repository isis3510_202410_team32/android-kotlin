package com.example.foodloop.ui.orders

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.DateRange
import androidx.compose.material.icons.filled.Done
import androidx.compose.material.icons.filled.List
import androidx.compose.material.icons.filled.LocationOn
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.Icon
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.foodloop.domain.Order
import com.example.foodloop.viewmodel.orders.OrderDetailViewModel
import com.example.foodloop.viewmodel.supermarkets.SupermarketDetailViewModel

@Composable
fun OrderDetailScreen(
    modifier: Modifier = Modifier,
    orderId:String,
    onHelpClick: () -> Unit,
    contentPadding: PaddingValues = PaddingValues(0.dp, 200.dp, 0.dp, 0.dp)
){
    val orderDetailViewModel: OrderDetailViewModel = viewModel()
    val data by orderDetailViewModel.orderData.collectAsState()

    LaunchedEffect(key1 = true) {
        orderDetailViewModel.fetchOrderData(orderId)
    }

    Column (
        modifier.fillMaxSize()
    ){
        Spacer(Modifier.height(42.dp))
        UpperBarOrders()
        Spacer(Modifier.height(42.dp))
        Description(data, modifier = Modifier, onHelpClick = onHelpClick)
        Spacer(Modifier.height(15.dp))
        OrderAddress(data)
        Spacer(Modifier.height(15.dp))
        OrderDate(data)
        Spacer(Modifier.height(15.dp))
        OrderCost(data)
        Spacer(Modifier.height(15.dp))
        OrderProducts(data)
    }
}

@Composable
fun Description(order: Order?, modifier: Modifier = Modifier, onHelpClick: () -> Unit) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = modifier
            .fillMaxWidth()
            //.padding(16.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .background(Color(0xFFF5F5F5))
            .padding(20.dp)
    ) {
        Column()
        {
            Text(
                text = "Order #",
                color = Color.Black,
                style = TextStyle(
                    fontSize = 14.sp
                )
            )
            Spacer(modifier = Modifier.height(10.dp))
            Text(
                text = order?.id?:"",
                color = Color.Black,
                style = TextStyle(
                    fontSize = 16.sp
                )
            )
        }
        OutlinedButton(onClick = onHelpClick) {
            Icon(
                imageVector = Icons.Default.Send,
                contentDescription = "help icon",
                tint = Color.Black
            )
            Spacer(modifier = Modifier.width(8.dp))
            Text(text = "Need help?")
        }
    }
}


@Composable
fun OrderAddress(order: Order?, modifier: Modifier = Modifier) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .background(Color(0xFFF5F5F5))
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(20.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.LocationOn,
                    contentDescription = "Location Icon",
                    tint = Color.Black,
                    modifier = Modifier
                        .size(30.dp)
                        .padding(end = 8.dp)
                )
                Column {
                    Text(
                        text = "Delivery address",
                        color = Color.Black,
                        style = TextStyle(
                            fontSize = 20.sp
                        )
                    )
                    Text(
                        text = order?.address?: " ",
                        color = Color.Gray,
                        style = TextStyle(
                            fontSize = 15.sp
                        )
                    )
                }
            }
        }
    }
}

@Composable
fun OrderDate(order: Order?, modifier: Modifier = Modifier){
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .background(Color(0xFFF5F5F5))
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(20.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.DateRange,
                    contentDescription = "Date Icon",
                    tint = Color.Black,
                    modifier = Modifier
                        .size(30.dp)
                        .padding(end = 8.dp)
                )
                Column {
                    Text(
                        text = "Delivery Date",
                        color = Color.Black,
                        style = TextStyle(
                            fontSize = 20.sp
                        )
                    )
                    Text(
                        text = order?.purchaseDate?: " ",
                        color = Color.Gray,
                        style = TextStyle(
                            fontSize = 15.sp
                        )
                    )
                }
            }
        }
    }
}

@Composable
fun OrderCost(order: Order?, modifier: Modifier = Modifier){
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .background(Color(0xFFF5F5F5))
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(20.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.Done,
                    contentDescription = "Icon",
                    tint = Color.Black,
                    modifier = Modifier
                        .size(30.dp)
                        .padding(end = 8.dp)
                )
                Column {
                    Text(
                        text = "Cost",
                        color = Color.Black,
                        style = TextStyle(
                            fontSize = 20.sp
                        )
                    )
                    Text(
                        text = order?.totalPrice.toString(),
                        color = Color.Gray,
                        style = TextStyle(
                            fontSize = 15.sp
                        )
                    )
                }
            }
        }
    }
}

@Composable
fun OrderProducts(order: Order?, modifier: Modifier = Modifier){
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = modifier
            .fillMaxWidth()
            .requiredHeight(84.dp)
            .clip(shape = RoundedCornerShape(8.dp))
            .background(Color(0xFFF5F5F5))
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.SpaceBetween,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row(
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier.padding(20.dp)
            ) {
                Icon(
                    imageVector = Icons.Default.List,
                    contentDescription = "List Icon",
                    tint = Color.Black,
                    modifier = Modifier
                        .size(30.dp)
                        .padding(end = 8.dp)
                )
                Column {
                    Text(
                        text = "Products",
                        color = Color.Black,
                        style = TextStyle(
                            fontSize = 20.sp
                        )
                    )
                    Text(
                        text = order?.products?.joinToString(", "){it.name}?: "",
                        color = Color.Gray,
                        style = TextStyle(
                            fontSize = 15.sp
                        )
                    )
                }
            }
        }
    }
}