package com.example.foodloop.ui

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavHostController
import coil.compose.rememberAsyncImagePainter
import com.example.foodloop.domain.PredictDao
import com.example.foodloop.domain.ProductDao
import com.example.foodloop.domain.SupermarketDao
import com.example.foodloop.model.Predict
import com.example.foodloop.model.Product
import com.example.foodloop.model.Supermarket
import com.example.foodloop.ui.theme.Green
import com.example.foodloop.ui.theme.GreenBackground
import com.example.foodloop.viewmodel.HomePageViewModel
import com.example.foodloop.viewmodel.HomePageViewModelFactory

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun HomePageScreen1(navController: NavHostController) {
    val scrollState = rememberScrollState()
    var searchText by remember { mutableStateOf("") }
    val context = LocalContext.current
    val application = context.applicationContext as Application

    val supermarketDao = remember { SupermarketDao() }
    val productDao = remember { ProductDao() }
    val predictDao = remember { PredictDao() }

    val viewModel: HomePageViewModel = viewModel(
        factory = HomePageViewModelFactory(
            application,
            supermarketDao,
            productDao,
            predictDao
        )
    )
    val supermarkets by viewModel.supermarkets.collectAsState()
    val products by viewModel.products.collectAsState()
    val predictions by viewModel.predictions.collectAsState()

    Column {
        Column {
            if (predictions != null) {
                PredictionsList(predictions)
            } else {
                Text("Loading predictions...", color = Color.White, modifier = Modifier.padding(16.dp))
            }
        }

        Column {
            if (supermarkets != null) {
                SupermarketsList(supermarkets)
            } else {
                Text("Loading supermarkets...", color = Color.White, modifier = Modifier.padding(16.dp))
            }
        }

        Column {
            if (products != null) {
                ProductListComposable(
                    products = products,
                    title = "Recommended Products"
                )
            } else {
                Text("Loading products...", color = Color.White, modifier = Modifier.padding(16.dp))
            }
        }

        Column {
            if (products != null) {
                ProductListComposable(
                    products = products,
                    title = "Recommended Products"
                )
            } else {
                Text("Loading products...", color = Color.White, modifier = Modifier.padding(16.dp))
            }
        }

        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = "Welcome to our application!",
                fontWeight = FontWeight.Bold,
                textAlign = TextAlign.Center,
                modifier = Modifier.fillMaxWidth()
            )
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun CustomTopAppBar() {
    TopAppBar(
        title = {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(text = "Food", color = Color.White, fontWeight = FontWeight.Bold)
                Text(text = "Loop", color = GreenBackground, fontWeight = FontWeight.Bold)
            }
        },
        navigationIcon = {
            IconButton(onClick = { }) {
                Icon(imageVector = Icons.Default.Menu, contentDescription = "Menu", tint = Color.White)
            }
        },
        actions = {
            IconButton(onClick = { }) {
                Icon(imageVector = Icons.Default.ShoppingCart, contentDescription = "Shopping Cart", tint = Color.White)
            }
        },
        colors = TopAppBarDefaults.topAppBarColors(containerColor = Green, titleContentColor = Color.White)
    )
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun SearchBar(searchText: String, onSearchTextChanged: (String) -> Unit, onSearchExecuted: () -> Unit) {
    val keyboardController = LocalSoftwareKeyboardController.current
    OutlinedTextField(
        value = searchText,
        onValueChange = onSearchTextChanged,
        modifier = Modifier
            .fillMaxWidth()
            .padding(8.dp),
        placeholder = { Text("Search for a product", color = Color.White) },
        leadingIcon = {
            Icon(imageVector = Icons.Default.Search, contentDescription = "Search", tint = Color.White)
        },
        colors = TextFieldDefaults.outlinedTextFieldColors(cursorColor = Color.White, focusedBorderColor = Green, unfocusedBorderColor = Green),
        shape = RoundedCornerShape(8.dp),
        keyboardOptions = KeyboardOptions.Default.copy(imeAction = ImeAction.Search),
        keyboardActions = KeyboardActions(onSearch = {
            onSearchExecuted()
            keyboardController?.hide()
        })
    )
}

@Composable
fun ShowUserId() {
    val context = LocalContext.current
    val sharedPreferences = remember { context.getSharedPreferences("user_prefs", Context.MODE_PRIVATE) }
    val userId = remember { sharedPreferences.getString("user_id", "No User ID Found") }

    Column(modifier = Modifier.padding(16.dp)) {
        Text(
            text = "User ID: $userId",
            modifier = Modifier.padding(8.dp)
        )
    }
}

@Composable
fun SupermarketsList(supermarkets: List<Supermarket>?) {
    Column(modifier = Modifier.background(Green)) {
        Text(
            "Supermarkets",
            modifier = Modifier.padding(start = 16.dp, top = 16.dp, bottom = 8.dp),
            color = Color.White,
            fontWeight = FontWeight.Bold
        )
        if (!supermarkets.isNullOrEmpty()) {
            LazyRow(
                contentPadding = PaddingValues(horizontal = 16.dp),
                horizontalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                items(supermarkets) { supermarket ->
                    SupermarketItem(supermarket)
                }
            }
        } else {
            Text("No supermarkets available.", Modifier.padding(16.dp), color = Color.White)
        }
    }
}

@Composable
fun SupermarketItem(supermarket: Supermarket) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        val context = LocalContext.current
        val isConnected = remember { checkInternetConnection(context) }

        if (isConnected) {
            val painter = rememberAsyncImagePainter(model = supermarket.imageUrl)
            Image(
                painter = painter,
                contentDescription = "${supermarket.name} Logo",
                modifier = Modifier
                    .size(100.dp)
                    .clickable { /* Action when item is clicked */ }
            )
        } else {
            Toast.makeText(context, "No internet", Toast.LENGTH_SHORT).show()
        }
        Text(
            text = supermarket.name,
            color = Color.White,
            fontWeight = FontWeight.Bold
        )
    }
}

@Composable
fun PredictionsList(predictions: List<Predict>?) {
    Column(modifier = Modifier.background(Green)) {
        Text(
            text = "Best Days!",
            modifier = Modifier.padding(start = 16.dp, top = 16.dp, bottom = 8.dp),
            color = Color.White,
            fontWeight = FontWeight.Bold
        )
        LazyRow(
            contentPadding = PaddingValues(horizontal = 16.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            items(predictions.orEmpty()) { predict ->
                PredictionItem(predict)
            }
        }
    }
}

@Composable
fun PredictionItem(predict: Predict) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Text(
            text = predict.date,
            color = Color.White,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(bottom = 4.dp)
        )
    }
}

@Composable
fun HeaderComposable(text: String, textColor: Color = Color.White) {
    Text(
        text = text,
        modifier = Modifier.padding(5.dp),
        color = textColor,
        fontWeight = FontWeight.Bold
    )
}

@Composable
fun ProductListComposable(
    products: List<Product>?,
    title: String
) {
    Column(modifier = Modifier.background(Color.White)) {
        HeaderComposable(text = title, textColor = Color.Black)
        if (!products.isNullOrEmpty()) {
            LazyRow(
                contentPadding = PaddingValues(horizontal = 5.dp),
                horizontalArrangement = Arrangement.spacedBy(16.dp)
            ) {
                items(products) { product ->
                    ProductItem(product = product)
                }
            }
        } else {
            Text("No products available.", modifier = Modifier.padding(16.dp), color = Color.Black)
        }
    }
}

@Composable
fun ProductItem(product: Product) {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        val context = LocalContext.current
        val isConnected = remember { checkInternetConnection(context) }

        if (isConnected) {
            val painter = rememberAsyncImagePainter(model = product.imageUrl)
            Image(
                painter = painter,
                contentDescription = product.name,
                modifier = Modifier
                    .size(100.dp)
                    .clickable { /* Action when item is clicked */ }
            )
        } else {
            Toast.makeText(context, "No internet", Toast.LENGTH_SHORT).show()
        }
        Text(
            text = product.name,
            modifier = Modifier.padding(top = 8.dp, bottom = 8.dp),
            color = Color.Black,
            fontWeight = FontWeight.Bold
        )
        PriceRow(originalPrice = product.price, discountedPrice = product.discountPrice)
    }
}

@Composable
fun PriceRow(originalPrice: String, discountedPrice: String) {
    Row {
        Text(
            text = originalPrice,
            modifier = Modifier.padding(end = 4.dp),
            color = Color.Gray,
            textDecoration = TextDecoration.LineThrough
        )
        Text(
            text = discountedPrice,
            fontWeight = FontWeight.Bold,
        )
    }
}

@Composable
fun ImageProductButtons(productImages: List<Int>, productNames: List<String>) {
    if (productImages.size != productNames.size) {
        throw IllegalArgumentException("The lists do not have the same length.")
    }
    Column(
        modifier = Modifier.padding(4.dp),
        verticalArrangement = Arrangement.spacedBy(4.dp)
    ) {
        productImages.indices.forEach { index ->
            Button(
                onClick = { /* Action */ },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 2.dp),
                colors = ButtonDefaults.buttonColors(containerColor = Color(0xFFF8F8F8)),
                contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp),
                shape = RectangleShape
            ) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.fillMaxWidth()
                ) {
                    Image(
                        painter = painterResource(id = productImages[index]),
                        contentDescription = "Image of ${productNames[index]}",
                        modifier = Modifier.size(40.dp)
                    )
                    Text(
                        text = productNames[index],
                        modifier = Modifier
                            .padding(start = 8.dp)
                            .align(Alignment.CenterVertically),
                        color = Color.Black
                    )
                }
            }
        }
    }
}

fun checkInternetConnection(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    return capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
}
