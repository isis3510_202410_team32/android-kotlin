package com.example.foodloop.ui
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.example.foodloop.ui.theme.Green
import androidx.compose.runtime.*
import com.example.foodloop.ui.search.Product


/*@Preview
@Composable
fun RecommendationsScreenPreview() {
    RecommendationsScreen()
}*/

@Composable
fun RecommendationsScreen(modifier: Modifier = Modifier) {
    var numProducts by remember { mutableStateOf(3) }
    Column(
        modifier = modifier
            .verticalScroll(rememberScrollState())
    ) {
        RecommendationLabel()
        Spacer(Modifier.height(25.dp))
        repeat(numProducts) {
            RowProducts()
            Spacer(Modifier.height(16.dp))
        }
        Spacer(Modifier.height(25.dp))
        if (numProducts < 12) {
            SearchButton(text = "Show More") { numProducts += 3 }
        }
    }
}


@Composable
fun RecommendationLabel(modifier: Modifier = Modifier) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .padding(0.dp)
            .background(color = Green),
    ) {
        Text(
            text = "Recommendations for you",
            color = Color.White,
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth()
        )
    }
}

@Composable
fun RowProducts() {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier.padding(horizontal = 20.dp),
    ) {
        Product(modifier = Modifier.padding(horizontal = 20.dp))
        Product(modifier = Modifier.padding(horizontal = 20.dp))
    }
}

@Composable
fun SearchButton(text: String, onClick: () -> Unit) {
    Box(
        modifier = Modifier.fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        ElevatedButton(onClick = onClick,
            ) {
            Text(text)
        }
    }
}





