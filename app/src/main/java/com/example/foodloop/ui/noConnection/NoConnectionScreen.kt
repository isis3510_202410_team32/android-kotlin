package com.example.foodloop.ui.noConnection

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.runtime.Composable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.material3.OutlinedButton
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.foodloop.R
import androidx.compose.foundation.layout.Box
import androidx.navigation.NavHostController
import com.example.foodloop.ui.theme.FoodLoopGreen



@Composable
fun NoConnection(onRetryClick: () -> Unit,
                 navController: NavHostController,
                 context: Context) {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(color = FoodLoopGreen)
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(16.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Image(
                painter = painterResource(id = R.drawable.logo),
                contentDescription = "There is not connectivity",
                modifier = Modifier.size(120.dp)
            )
            Text(
                text = "There is not connectivity",
                style = TextStyle(fontSize = 18.sp),
                modifier = Modifier.padding(top = 16.dp)
            )
            OutlinedButton(
                onClick = { if (isInternetConnected(connectivityManager)) {
                    navController.navigate("Savings")
                } else {
                    onRetryClick()
                }

                },
                modifier = Modifier.padding(top = 16.dp)
            ) {
                Text(text = "Retry")
            }
        }
    }
}

private fun isInternetConnected(connectivityManager: ConnectivityManager): Boolean {
    val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    return capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
}
