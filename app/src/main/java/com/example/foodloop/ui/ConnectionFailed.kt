package com.example.foodloop.ui

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import com.example.foodloop.FoodloopScreen
import com.example.foodloop.R
import com.example.foodloop.ui.theme.GreenBackground
import com.example.foodloop.ui.theme.Green
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

@Composable
fun ConnectionFailed(navController: NavHostController, context: Context) {
    val image = painterResource(id = R.drawable.logo)
    val snackbarHostState = remember { SnackbarHostState() }
    val sharedPreferences = context.getSharedPreferences("AppPreferences", Context.MODE_PRIVATE)
    var isConnected by remember { mutableStateOf(sharedPreferences.getBoolean("LastInternetStatus", false)) }
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    // Check connectivity asynchronously and handle caching
    LaunchedEffect(key1 = Unit) {
        isConnected = withContext(Dispatchers.IO) {
            checkInternetConnection(connectivityManager, sharedPreferences)
        }
    }

    var showSnackbar by remember { mutableStateOf(false) }

    LaunchedEffect(showSnackbar) {
        if (showSnackbar) {
            snackbarHostState.showSnackbar("No internet connection")
            showSnackbar = false
        }
    }

    Box(modifier = Modifier.fillMaxSize()) {
        Surface(
            modifier = Modifier.fillMaxSize(),
            color = GreenBackground
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .padding(16.dp),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                Image(
                    painter = image,
                    contentDescription = "Logo"
                )
                Spacer(modifier = Modifier.height(32.dp))
                Text(
                    "No internet connection :(",
                    style = MaterialTheme.typography.headlineMedium,
                    textAlign = TextAlign.Center,
                    color = Green
                )
                Spacer(modifier = Modifier.height(32.dp))

                Button(
                    onClick = {
                        if (!isConnected) {
                            showSnackbar = true
                        } else {
                            navController.navigate(FoodloopScreen.HomePage.name)
                        }
                    },
                    colors = ButtonDefaults.buttonColors(containerColor = Green),
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(48.dp)
                        .border(1.dp, Green, shape = RectangleShape),
                    shape = RectangleShape,
                    contentPadding = PaddingValues(),
                ) {
                    Text("Reconnect", color = Color.White)
                }

                Spacer(modifier = Modifier.height(16.dp))
            }
        }

        SnackbarHost(
            hostState = snackbarHostState,
            modifier = Modifier.align(Alignment.BottomCenter)
        ) { data ->
            Snackbar(
                snackbarData = data,
                containerColor = Color.White,
                contentColor = Color.Black,
                actionColor = Color.Black
            )
        }
    }
}
