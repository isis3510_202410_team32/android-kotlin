package com.example.foodloop.ui.supermarkets

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.foodloop.ui.theme.Green
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.foodloop.ui.theme.GreenTwo
import com.example.foodloop.viewmodel.supermarkets.SupermarketDetailViewModel
import com.example.foodloop.viewmodel.supermarkets.SupermarketViewModel
import java.util.Calendar

@Preview
@Composable
fun SupermarketDetailPreview() {
    SupermarketDetail("")
}

@Composable
fun SupermarketDetail(storeId: String) {
    val supermarketDetailViewModel: SupermarketDetailViewModel = viewModel()
    val data by supermarketDetailViewModel.supermarketData.collectAsState()

    LaunchedEffect(key1 = true) {
        supermarketDetailViewModel.fetchSupermarketData(storeId)
    }

    Column (){
        Spacer(modifier = Modifier.height(42.dp))
        TitleBar()
        Spacer(modifier = Modifier.height(42.dp))
        Text(
            text = data?.name?: "",
            textAlign = TextAlign.Center,
            modifier = Modifier.fillMaxWidth(),
            style = TextStyle(
                fontSize = 30.sp,
                fontWeight = FontWeight.Bold,
                color = GreenTwo
            )
        )
        if(data?.name != null) {
            Spacer(modifier = Modifier.height(40.dp))
            Row(
                modifier = Modifier.fillMaxWidth().wrapContentSize(Alignment.Center)
            ){
                ImageFromUrl(
                    url = data?.imageUrl ?: "", contentDescription = "Supermarket image"
                )
            }
        }
        Spacer(modifier = Modifier.height(40.dp))
        Address(data?.address?: "")
        Spacer(modifier = Modifier.height(25.dp))
    }
}



@Composable
fun TitleBar (modifier: Modifier = Modifier) {
    NavigationBar(
        containerColor = Green,
        modifier = modifier.requiredHeight(36.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {

            Text(
                text = "Supermarkets",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 120.dp),
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White
            )
        }
    }
}

@Composable
fun Address(address: String){
    Text(
        text = address,
        textAlign = TextAlign.Center,
        modifier = Modifier.fillMaxWidth(),
        style = TextStyle(
            fontSize = 25.sp,
            fontWeight = FontWeight.Bold
        )
    )
}


