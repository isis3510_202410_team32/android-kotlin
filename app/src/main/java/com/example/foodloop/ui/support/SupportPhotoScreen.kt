package com.example.foodloop.ui.support

import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.util.Log
import android.widget.Toast
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.requiredHeight
import androidx.compose.foundation.layout.width
import androidx.compose.material3.ElevatedButton
import androidx.compose.material3.Icon
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import coil.compose.rememberAsyncImagePainter
import com.example.foodloop.R
import com.example.foodloop.ui.theme.Green
import java.io.File
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Objects



@Composable
fun SupportPhotoScreen(modifier: Modifier = Modifier.padding(0.dp, 0.dp, 0.dp, 0.dp) ) {
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val context = LocalContext.current
    ///storage/emulated/0/Picture

    val storageDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
    Log.d("Archivos", storageDir?.path ?: ":o")
    val newFile = File(storageDir,  timeStamp + ".jpg")
    Log.d("Archivos", newFile.path)

    val uri = FileProvider.getUriForFile(
        Objects.requireNonNull(context),
        "com.example.foodloop.provider", newFile
    )

    var capturedImageUri by remember {
        mutableStateOf<Uri>(Uri.EMPTY)
    }

    val cameraLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.TakePicture()) {
            capturedImageUri = uri
            Log.d("Archivos", "capturedImageUri: ${newFile.path}")
        }

    val permissionLauncher = rememberLauncherForActivityResult(
        ActivityResultContracts.RequestPermission()
    ) {
        if (it) {
            Toast.makeText(context, "Permission Granted", Toast.LENGTH_SHORT).show()
            cameraLauncher.launch(uri)
        } else {
            Toast.makeText(context, "Permission Denied", Toast.LENGTH_SHORT).show()
        }
    }

    Column(
        Modifier
            .fillMaxSize(),
        //.padding(100.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(44.dp))
        UpperBar()
        Spacer(modifier = Modifier.height(25.dp))
        ElevatedButton(onClick = {
            val permissionCheckResult =
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.CAMERA)
            if (permissionCheckResult == PackageManager.PERMISSION_GRANTED) {
                cameraLauncher.launch(uri)
            } else {
                // Request a permission
                permissionLauncher.launch(android.Manifest.permission.CAMERA)
            }
        }) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                Text(text = "Take a photo of the product")
                Spacer(modifier = Modifier.width(8.dp))
                Icon(contentDescription = "Camera",painter = painterResource(id = R.drawable.camara))
            }
        }
    }

    if (capturedImageUri.path?.isNotEmpty() == true) {
        Log.d("Archivos", "capturedImageUri2: ${newFile.path}")
        Image(
            modifier = Modifier
                .padding(80.dp, 8.dp),
            painter = rememberAsyncImagePainter(capturedImageUri),
            contentDescription = null
        )
    }
}
@Composable
fun UpperBar(modifier: Modifier = Modifier) {
    NavigationBar(
        containerColor = Green,
        modifier = modifier.requiredHeight(36.dp)
    ) {
        Row(
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = "Support",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 150.dp),
                fontSize = 24.sp,
                fontWeight = FontWeight.Bold,
                color = Color.White
            )
        }
    }
}
