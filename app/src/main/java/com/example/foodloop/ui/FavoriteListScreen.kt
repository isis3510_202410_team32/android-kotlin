package com.example.foodloop.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme.typography
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.foodloop.R
import com.example.foodloop.model.Product
import com.example.foodloop.ui.theme.Green


@Preview(showSystemUi = true)
@Composable
fun FavoriteListScreen() {

    val favoriteProducts = listOf(
        Product(
            "Agua con gas",
            "4500",
            "20/04/2024",
            "https://donjacobo-1c705.kxcdn.com/web/image/product.product/141/image_1024/AGUA%20CON%20GAS?unique=73b76b8",
            "Agua con gas",
            "5000"
        ),
        Product(
            "Coca cola",
            "2500",
            "30/03/2024",
            "https://lavaquita.co/cdn/shop/products/supermercados_la_vaquita_supervaquita_gaseosa_coca_cola_10_oz_bebidas_liquidas_700x700.jpg?v=1620489361",
            "Coca cola",
            "3000"
        )
    )

    LazyColumn(
        contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp),
        modifier = Modifier.padding(top = 64.dp)
    ) {
        items(
            items = favoriteProducts,
            itemContent = {
                FavoriteListItem(product = it)
            }
        )
    }
}


@Composable
fun TitleScreen() {
    Box(modifier = Modifier.background(Green)) {
        Text(
            text = "Your favorites",
            style = typography.h4,
            color = Color.White,
            textAlign = TextAlign.Center,
        )
    }
}

@Composable
fun FavoriteListItem(product: Product) {
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .fillMaxWidth(),
        elevation = CardDefaults.cardElevation(
            defaultElevation = 2.dp
        ),
        shape = RoundedCornerShape(corner = CornerSize(16.dp))
    ) {
        Row(modifier = Modifier.fillMaxWidth()) {
            ProductImage(product = product)
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically)
            ) {
                Text(text = product.name, style = typography.h6)
                Row {
                    Text(
                        text = "$" + product.discountPrice,
                        fontWeight = FontWeight.Bold
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Text(
                        text = "$" + product.price,
                        textDecoration = TextDecoration.LineThrough
                    )
                    Spacer(modifier = Modifier.width(16.dp))
                    Text(text = getDiscount(product) + "%")
                    Spacer(modifier = Modifier.width(16.dp))
                    FavoriteIcon()
                }
            }

        }
    }
}

@Composable
private fun ProductImage(product: Product) {
    Image(
        painter = painterResource(id = R.drawable.ic_aguacoco),
        contentDescription = null,
        contentScale = ContentScale.Crop,
        modifier = Modifier
            .padding(8.dp)
            .size(84.dp)
            .clip(RoundedCornerShape(corner = CornerSize(16.dp)))
    )
}

fun getDiscount(product: Product): String {
    var priceDifference = product.price.toInt() - product.discountPrice.toInt();
    println(priceDifference)
    var percentageDiscount = (priceDifference / product.price.toInt()) * 100
    println(percentageDiscount)
    return percentageDiscount.toString()
}

@Composable
fun FavoriteIcon() {

    var stateFavorite by rememberSaveable { mutableStateOf(true) }

    Image(painter = painterResource(id = R.drawable.ic_fav), contentDescription = "Favorite",
        modifier = Modifier.clickable { stateFavorite = false })
}