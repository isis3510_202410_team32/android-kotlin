import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.example.foodloop.R
import com.example.foodloop.ui.Recipe1
import com.example.foodloop.ui.TopBar
import com.example.foodloop.ui.theme.Green

@Composable
fun RecipeScreen() {
    var selectedTab by remember { mutableStateOf(0) }
    val imagePainter = painterResource(id = R.drawable.pasta)

    val spaghettiCarbonaraRecipe = Recipe1(
        title = "Spaghetti Carbonara",
        difficulty = "Medium",
        calories = 600,
        time = "30m",
        imagePainter = imagePainter,
        description = "A classic Italian pasta dish with eggs, cheese, pancetta, and black pepper."
    )

    val tabTitles = listOf("Ingredients", "Tutorial")

    Column(modifier = Modifier.fillMaxSize()) {
        TopBar()

        RecipeTopSection(recipe = spaghettiCarbonaraRecipe)

        Box(
            contentAlignment = Alignment.Center
        ) {
            ScrollableTabRow(selectedTabIndex = selectedTab, modifier = Modifier.fillMaxWidth()) {
                tabTitles.forEachIndexed { index, title ->
                    Tab(
                        selected = selectedTab == index,
                        onClick = { selectedTab = index },
                        text = { Text(title) }
                    )
                }
            }
        }

        val productRecommendedImages = listOf(R.drawable.margarita_natural, R.drawable.margarita_natural, R.drawable.margarita_natural, R.drawable.margarita_natural)
        val productRecommendedNames = listOf("Margarita", "Agua de Coco", "Agua de Coco", "Agua de Coco")

        when (selectedTab) {
            0 -> IngredientsTab(productRecommendedNames)
            1 -> TutorialTab()
            // La pestaña 2 no estaba definida en tu arreglo original, entonces la omito para evitar confusión
        }
    }
}


@Composable
fun RecipeTopSection(recipe: Recipe1) {
    Column {
        Image(
            painter = recipe.imagePainter,
            contentDescription = "Recipe Image"
        )
        Surface(
            color = Green, // Fondo verde
            modifier = Modifier.fillMaxWidth() // Ocupa todo el ancho
        ) {
            Column(
                modifier = Modifier.padding(16.dp) // Añadir un relleno
            ) {
                Text(
                    recipe.title,
                    style = TextStyle(fontWeight = FontWeight.Bold, fontSize = 20.sp),
                    color = Color.White // Texto blanco
                )
                Text(
                    "${recipe.calories} Cal • ${recipe.time}",
                    style = TextStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp),
                    color = Color.White // Texto blanco
                )
                Text(
                    "Difficulty: ${recipe.difficulty}",
                    style = TextStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp),
                    color = Color.White // Texto blanco
                )
                Text(
                    text = "Description: ${recipe.description ?: "No description"}",
                    style = TextStyle(fontWeight = FontWeight.Bold, fontSize = 16.sp),
                    color = Color.White // Texto blanco
                )
            }
        }
    }
}



@Composable
fun IngredientsTab(ingredients: List<String>) {
    Column {
        ingredients.forEach { ingredient ->
            Button(
                onClick = { /* Acción */ },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 2.dp), //
                colors = ButtonDefaults.buttonColors(containerColor =  Color(0xFFF8F8F8)),
                contentPadding = PaddingValues(horizontal = 16.dp, vertical = 8.dp),
                shape = RectangleShape
            ) {
                Text(
                    text = ingredient,
                    color = Color.Black
                )
            }
        }
    }
}


@Composable
fun TutorialTab() {
    Text("Tutorial content goes here...")
}

@Preview(showBackground = true)
@Composable
fun PreviewRecipeScreen() {
    RecipeScreen()
}
