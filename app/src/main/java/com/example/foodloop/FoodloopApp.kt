package com.example.foodloop

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.util.Log
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Menu
import androidx.compose.material.icons.outlined.ShoppingCart
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBar
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.compose.rememberNavController
import com.example.foodloop.ui.AuthenticationScreen
import com.example.foodloop.ui.ConnectionFailed
import com.example.foodloop.ui.FavoriteListScreen
import com.example.foodloop.ui.HomePageScreen1
import com.example.foodloop.ui.LoginScreen
import com.example.foodloop.ui.MealScreen
import com.example.foodloop.ui.RecommendationsScreen
import com.example.foodloop.ui.RegisterScreen
import com.example.foodloop.ui.WasteList
import com.example.foodloop.ui.navigationDrawer.NavigationDrawer
import com.example.foodloop.ui.noConnection.NoConnection
import com.example.foodloop.ui.savings.SavingScreen
import com.example.foodloop.ui.search.SearchProductScreen
import com.example.foodloop.ui.search.SearchScreen
import com.example.foodloop.ui.supermarkets.SupermarketDetail
import com.example.foodloop.ui.supermarkets.SuppermarketList
import com.example.foodloop.ui.support.SupportHomeScreen
import com.example.foodloop.ui.support.SupportPhotoScreen
import com.example.foodloop.ui.support.SupportProblemScreen
import com.example.foodloop.ui.theme.Green
import com.example.foodloop.ui.theme.LightGreen


enum class FoodloopScreen() {
    Search,
    SearchProduct,
    Recommendations,
    Savings,
    SupportHome,
    SupportProblem,
    SupportPhoto,
    NavigationDrawer,
    Register,
    Login,
    Authentication,
    HomePage,
    FavoriteList,
    Supermarket,
    NoConnection,
    ConnectionFailed,
    SupermarketDetail,
    MyMeals,
    RecipeList

}

@Composable
fun FoodloopApp(
    viewModel: MainViewModel,
    navController: NavHostController = rememberNavController()
) {
    val context = LocalContext.current
    val drawerState = remember { mutableStateOf(false) }
    Scaffold(
        topBar = {
            FoodloopNavigationBar(
                context = context,
                modifier = Modifier
                    .padding(0.dp)
                    .height(44.dp)
                    .background(Color.Yellow),
                navController = navController
            )
        }
    ) {
            innerPadding ->  {}
        NavHost(
            navController = navController,
            startDestination = FoodloopScreen.Login.name,
            modifier = Modifier
                .fillMaxSize()
                .background(Color.White)
        ) {
            Log.d("Login", "FirstRun")
            composable(route = FoodloopScreen.SearchProduct.name) {
                SearchProductScreen(
                    viewModel,
                    onProductButtonClicked = { navController.navigate(FoodloopScreen.Search.name) },
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            }
            composable(route = FoodloopScreen.Search.name) {
                SearchScreen(
                    viewModel,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            }
            composable(route = FoodloopScreen.Savings.name) {
                SavingScreen(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            }
            composable(route = FoodloopScreen.SupportPhoto.name) {
                SupportPhotoScreen(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            }

            composable(route = FoodloopScreen.ConnectionFailed.name) {
                ConnectionFailed(navController = navController, context = LocalContext.current)
            }
            composable(route = FoodloopScreen.SupportHome.name) {
                SupportHomeScreen(
                    onOrderButtonClicked = { navController.navigate(FoodloopScreen.SupportProblem.name) },
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            }
            composable(route = FoodloopScreen.SupportProblem.name) {
                SupportProblemScreen(
                    onProblemButtonClicked = { navController.navigate(FoodloopScreen.SupportPhoto.name) },
                    onOtherProblemButtonClicked = { navController.navigate(FoodloopScreen.SupportPhoto.name) },
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            }

            composable(route = FoodloopScreen.NavigationDrawer.name) {
                //val navController = rememberNavController()
                val context = LocalContext.current
                NavigationDrawer(
                    navController = navController,
                    onSupportClick = { navController.navigate(FoodloopScreen.SupportProblem.name) },
                    onSavingsClick = { navController.navigate(FoodloopScreen.Savings.name) },
                    onLogoutClick = { navController.navigate(FoodloopScreen.Login.name) },
                    onFavoriteListClick = { navController.navigate(FoodloopScreen.FavoriteList.name)},
                    onSupermarketClick = { navController.navigate(FoodloopScreen.Supermarket.name)},
                    onMealsClick = { navController.navigate(FoodloopScreen.MyMeals.name)},
                    onRecipesClick = { navController.navigate(FoodloopScreen.RecipeList.name)},
                    context = context
                )
            }

            composable(route = FoodloopScreen.Recommendations.name) {
                RecommendationsScreen(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            }
            composable(route = FoodloopScreen.Register.name) {
                RegisterScreen(navController = navController)
            }
            composable(route = FoodloopScreen.Login.name) {
                LoginScreen(navController = navController, context = LocalContext.current)
            }
            composable(route = FoodloopScreen.Authentication.name) {
                AuthenticationScreen(navController = navController)
            }

            composable(route = FoodloopScreen.HomePage.name) {
                HomePageScreen1(navController = navController)
            }

            composable(route = FoodloopScreen.FavoriteList.name){
                FavoriteListScreen()
            }

            composable(route = FoodloopScreen.RecipeList.name){
                WasteList()
            }

            composable(route = FoodloopScreen.MyMeals.name){
                MealScreen()
            }

            composable(route = FoodloopScreen.Supermarket.name) {
                SuppermarketList(
                    onSupermarketClick = {
                        navController.navigate("${FoodloopScreen.SupermarketDetail.name}/$it")
                    },
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(innerPadding)
                )
            }
            composable(route = FoodloopScreen.NoConnection.name){
                val context = LocalContext.current
                NoConnection(
                    navController = navController,
                    context = context,
                    onRetryClick = {},

                    )
            }

            composable(route = "${FoodloopScreen.SupermarketDetail.name}/{storeId}") {
                val storeId = it.arguments?.getString("storeId") ?: ""
                SupermarketDetail(storeId)
            }

        }
    }
}

@Composable
fun FoodloopNavigationBar(
    context: Context,
    modifier: Modifier = Modifier,
    navController: NavHostController
) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()

    NavigationBar(
        containerColor = Green,
        modifier = modifier
    ) {
        NavigationBarItem(
            icon = {
                Icon(
                    imageVector = Icons.Default.Menu,
                    contentDescription = null,
                    tint = Color.White
                )
            },
            selected = false,
            onClick = {
                if(navBackStackEntry?.destination?.route == FoodloopScreen.NavigationDrawer.name){
                    navController.popBackStack()
                } else {
                    navController.navigate(FoodloopScreen.NavigationDrawer.name)
                }
            }
        )
        Text(
            text = AnnotatedString.Builder().apply {
                withStyle(style = SpanStyle(color = Color.White)) {
                    append("Food")
                }
                withStyle(style = SpanStyle(color = LightGreen)) {
                    append("loop")
                }
            }.toAnnotatedString(),
            modifier = Modifier
                .fillMaxWidth(0.7F)
                .clickable {
                    navController.navigate(FoodloopScreen.HomePage.name)
                },
            textAlign = TextAlign.Center,
            style = MaterialTheme.typography.headlineMedium
        )

        // Shopping cart with connectivity check
        NavigationBarItem(
            icon = {
                Box(
                    modifier = Modifier
                        .padding(10.dp)
                        .background(Color.White, RoundedCornerShape(4.dp))
                ) {
                    Icon(
                        imageVector = Icons.Outlined.ShoppingCart,
                        contentDescription = "Shopping Cart",
                        tint = Color.Black
                    )
                }
            },
            selected = false,
            onClick = {
                if (checkInternetConnection(context)) {
                    Log.d("InternetCheck", "Internet connection is available.")
                    // Optionally navigate or do other actions
                } else {
                    navController.navigate(FoodloopScreen.ConnectionFailed.name)
                }
            }
        )
    }
}

private fun checkInternetConnection(context: Context): Boolean {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val capabilities = connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
    return capabilities?.hasCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET) ?: false
}

