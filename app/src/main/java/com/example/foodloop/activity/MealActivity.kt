package com.example.foodloop.activity

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.lifecycle.ViewModelProvider
import com.example.foodloop.activity.ui.theme.FoodloopTheme
import com.example.foodloop.viewmodel.MealViewModel

class MealActivity : ComponentActivity() {
    private val viewModel: MealViewModel by viewModels{
        ViewModelProvider.AndroidViewModelFactory.getInstance(application)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FoodloopTheme {
                Surface(color = MaterialTheme.colorScheme.background) {
                    //MealScreen(viewModel)
                }
            }
        }
    }
}
